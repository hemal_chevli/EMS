EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Dispositivos_I2C
LIBS:DataStorage
LIBS:ESP8266
LIBS:TransistorParts
LIBS:EnergyMonitorSystem-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Energy Monitoring System"
Date "2015-09-26"
Rev "0.0"
Comp "Black Electronics [INDIA]"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATMEGA2560-A IC1
U 1 1 56064657
P 6450 3700
F 0 "IC1" H 5300 6500 40  0000 L BNN
F 1 "ATMEGA2560-A" H 7150 850 40  0000 L BNN
F 2 "QFP:TQFP-100_14x14mm_Pitch0.5mm" H 6450 3700 30  0000 C CIN
F 3 "" H 6450 3700 60  0000 C CNN
	1    6450 3700
	1    0    0    -1  
$EndComp
$Comp
L AP1117 U3
U 1 1 56064A6F
P 1900 900
F 0 "U3" H 2000 650 50  0000 C CNN
F 1 "1117-5" H 1900 1150 50  0000 C CNN
F 2 "sot223:SOT223" H 1900 900 60  0001 C CNN
F 3 "" H 1900 900 60  0000 C CNN
	1    1900 900 
	1    0    0    -1  
$EndComp
$Comp
L AP1117 U1
U 1 1 56064AB7
P 1500 1600
F 0 "U1" H 1600 1350 50  0000 C CNN
F 1 "1117-3V3" H 1500 1850 50  0000 C CNN
F 2 "sot223:SOT223" H 1500 1600 60  0001 C CNN
F 3 "" H 1500 1600 60  0000 C CNN
	1    1500 1600
	1    0    0    -1  
$EndComp
$Comp
L LED D2
U 1 1 56064C13
P 2400 2600
F 0 "D2" H 2400 2700 50  0000 C CNN
F 1 "PWR" H 2400 2500 50  0000 C CNN
F 2 "LEDs:LED-0805" H 2400 2600 60  0001 C CNN
F 3 "" H 2400 2600 60  0000 C CNN
	1    2400 2600
	0    -1   -1   0   
$EndComp
$Comp
L D D1
U 1 1 56064D0A
P 1250 900
F 0 "D1" H 1250 1000 50  0000 C CNN
F 1 "D" H 1250 800 50  0000 C CNN
F 2 "Diodes_SMD:SMA_Standard" H 1250 900 60  0001 C CNN
F 3 "" H 1250 900 60  0000 C CNN
	1    1250 900 
	-1   0    0    1   
$EndComp
$Comp
L CP C8
U 1 1 5606481B
P 2350 1100
F 0 "C8" H 2375 1200 50  0000 L CNN
F 1 "10uF" H 2375 1000 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeB_EIA-3528_Reflow" H 2388 950 30  0001 C CNN
F 3 "" H 2350 1100 60  0000 C CNN
	1    2350 1100
	1    0    0    -1  
$EndComp
$Comp
L Crystal_Small Y1
U 1 1 56064845
P 4900 1550
F 0 "Y1" H 4900 1650 50  0000 C CNN
F 1 "16MHz" H 4900 1450 50  0000 C CNN
F 2 "Crystals:Crystal_HC50-U_Vertical" H 4900 1550 60  0001 C CNN
F 3 "" H 4900 1550 60  0000 C CNN
	1    4900 1550
	0    1    1    0   
$EndComp
$Comp
L C_Small C10
U 1 1 56064908
P 4550 1400
F 0 "C10" H 4560 1470 50  0000 L CNN
F 1 "22pF" H 4450 1250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4550 1400 60  0001 C CNN
F 3 "" H 4550 1400 60  0000 C CNN
	1    4550 1400
	0    1    1    0   
$EndComp
$Comp
L C_Small C11
U 1 1 5606493D
P 4550 1700
F 0 "C11" H 4560 1770 50  0000 L CNN
F 1 "22pF" H 4560 1620 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4550 1700 60  0001 C CNN
F 3 "" H 4550 1700 60  0000 C CNN
	1    4550 1700
	0    1    1    0   
$EndComp
$Comp
L R R8
U 1 1 56064B97
P 4500 800
F 0 "R8" V 4580 800 50  0000 C CNN
F 1 "10K" V 4500 800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 4430 800 30  0001 C CNN
F 3 "" H 4500 800 30  0000 C CNN
	1    4500 800 
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 5606552C
P 2400 2200
F 0 "R6" V 2480 2200 50  0000 C CNN
F 1 "10K" V 2400 2200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2330 2200 30  0001 C CNN
F 3 "" H 2400 2200 30  0000 C CNN
	1    2400 2200
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR01
U 1 1 5606582F
P 1500 2000
F 0 "#PWR01" H 1500 1750 50  0001 C CNN
F 1 "GND" H 1500 1850 50  0000 C CNN
F 2 "" H 1500 2000 60  0000 C CNN
F 3 "" H 1500 2000 60  0000 C CNN
	1    1500 2000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 560658EB
P 2400 2850
F 0 "#PWR02" H 2400 2600 50  0001 C CNN
F 1 "GND" H 2400 2700 50  0000 C CNN
F 2 "" H 2400 2850 60  0000 C CNN
F 3 "" H 2400 2850 60  0000 C CNN
	1    2400 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 560659EF
P 2300 1300
F 0 "#PWR03" H 2300 1050 50  0001 C CNN
F 1 "GND" H 2300 1150 50  0000 C CNN
F 2 "" H 2300 1300 60  0000 C CNN
F 3 "" H 2300 1300 60  0000 C CNN
	1    2300 1300
	1    0    0    -1  
$EndComp
Text Label 2350 900  0    60   ~ 0
5V
Text Label 2250 2050 0    60   ~ 0
5V
Text Label 1000 1600 0    60   ~ 0
5V
$Comp
L CP C6
U 1 1 560F76EC
P 1900 1800
F 0 "C6" H 1925 1900 50  0000 L CNN
F 1 "10uF" H 1925 1700 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeB_EIA-3528_Reflow" H 1938 1650 30  0001 C CNN
F 3 "" H 1900 1800 60  0000 C CNN
	1    1900 1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 560F8748
P 4300 1650
F 0 "#PWR04" H 4300 1400 50  0001 C CNN
F 1 "GND" H 4300 1500 50  0000 C CNN
F 2 "" H 4300 1650 60  0000 C CNN
F 3 "" H 4300 1650 60  0000 C CNN
	1    4300 1650
	1    0    0    -1  
$EndComp
Text Label 5950 750  0    60   ~ 0
5V
$Comp
L GND #PWR05
U 1 1 560F9D28
P 6450 6800
F 0 "#PWR05" H 6450 6550 50  0001 C CNN
F 1 "GND" H 6450 6650 50  0000 C CNN
F 2 "" H 6450 6800 60  0000 C CNN
F 3 "" H 6450 6800 60  0000 C CNN
	1    6450 6800
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X03 P4
U 1 1 560FC868
P 1550 7400
F 0 "P4" H 1550 7600 50  0000 C CNN
F 1 "ISP" H 1550 7200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" H 1550 6200 60  0001 C CNN
F 3 "" H 1550 6200 60  0000 C CNN
	1    1550 7400
	1    0    0    -1  
$EndComp
Text Label 2050 7300 0    60   ~ 0
5V
$Comp
L GND #PWR06
U 1 1 560FD162
P 2050 7500
F 0 "#PWR06" H 2050 7250 50  0001 C CNN
F 1 "GND" H 2050 7350 50  0000 C CNN
F 2 "" H 2050 7500 60  0000 C CNN
F 3 "" H 2050 7500 60  0000 C CNN
	1    2050 7500
	1    0    0    -1  
$EndComp
Text Label 2050 7400 0    60   ~ 0
MOSI_5V
Text Label 800  7300 0    60   ~ 0
MISO_5V
Text Label 800  7400 0    60   ~ 0
SCK_5V
Text Label 1050 7500 0    60   ~ 0
~R
Text Label 4950 1000 0    60   ~ 0
~R
Text Label 8100 2200 0    60   ~ 0
MOSI_5V
Text Label 8100 2100 0    60   ~ 0
SCK_5V
Text Label 8100 2300 0    60   ~ 0
MISO_5V
$Comp
L CP C4
U 1 1 560647E1
P 1500 1100
F 0 "C4" H 1525 1200 50  0000 L CNN
F 1 "100uF" H 1525 1000 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeB_EIA-3528_Reflow" H 1538 950 30  0001 C CNN
F 3 "" H 1500 1100 60  0000 C CNN
	1    1500 1100
	1    0    0    -1  
$EndComp
Text Label 1400 900  0    60   ~ 0
12V
$Comp
L CONN_01X02 P7
U 1 1 56103105
P 3800 7450
F 0 "P7" H 3800 7600 50  0000 C CNN
F 1 "FAN" V 3900 7450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 3800 7450 60  0001 C CNN
F 3 "" H 3800 7450 60  0000 C CNN
	1    3800 7450
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 56103660
P 4250 7500
F 0 "#PWR07" H 4250 7250 50  0001 C CNN
F 1 "GND" H 4250 7350 50  0000 C CNN
F 2 "" H 4250 7500 60  0000 C CNN
F 3 "" H 4250 7500 60  0000 C CNN
	1    4250 7500
	1    0    0    -1  
$EndComp
Text Label 4250 7400 0    60   ~ 0
12V
$Comp
L SW_PUSH SW1
U 1 1 5610560E
P 4050 1100
F 0 "SW1" H 4050 1100 50  0000 C CNN
F 1 "~R" H 4050 1250 50  0000 C CNN
F 2 "IHE:SW_M6_PTH" H 4050 1100 60  0001 C CNN
F 3 "" H 4050 1100 60  0000 C CNN
	1    4050 1100
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 56107398
P 1200 2600
F 0 "C1" H 1225 2700 50  0000 L CNN
F 1 "10nF" H 1225 2500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1238 2450 30  0001 C CNN
F 3 "" H 1200 2600 60  0000 C CNN
	1    1200 2600
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 56107A89
P 1450 2600
F 0 "C3" H 1475 2700 50  0000 L CNN
F 1 "10nF" H 1475 2500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1488 2450 30  0001 C CNN
F 3 "" H 1450 2600 60  0000 C CNN
	1    1450 2600
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 56107ADC
P 1700 2600
F 0 "C5" H 1725 2700 50  0000 L CNN
F 1 "10nF" H 1725 2500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1738 2450 30  0001 C CNN
F 3 "" H 1700 2600 60  0000 C CNN
	1    1700 2600
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 56107B34
P 1950 2600
F 0 "C7" H 1975 2700 50  0000 L CNN
F 1 "10nF" H 1975 2500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1988 2450 30  0001 C CNN
F 3 "" H 1950 2600 60  0000 C CNN
	1    1950 2600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 56108331
P 1600 2900
F 0 "#PWR08" H 1600 2650 50  0001 C CNN
F 1 "GND" H 1600 2750 50  0000 C CNN
F 2 "" H 1600 2900 60  0000 C CNN
F 3 "" H 1600 2900 60  0000 C CNN
	1    1600 2900
	1    0    0    -1  
$EndComp
Text Label 1000 2350 0    60   ~ 0
5V
Text Notes 1200 2300 0    60   ~ 0
Decoupling Caps
$Comp
L GND #PWR09
U 1 1 5610C297
P 3600 1100
F 0 "#PWR09" H 3600 850 50  0001 C CNN
F 1 "GND" H 3600 950 50  0000 C CNN
F 2 "" H 3600 1100 60  0000 C CNN
F 3 "" H 3600 1100 60  0000 C CNN
	1    3600 1100
	1    0    0    -1  
$EndComp
Text Label 4150 800  0    60   ~ 0
5V
$Comp
L C_Small C9
U 1 1 5610C527
P 4050 1250
F 0 "C9" V 4000 1350 50  0000 L CNN
F 1 "10nF" V 4000 1000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4050 1250 60  0001 C CNN
F 3 "" H 4050 1250 60  0000 C CNN
	1    4050 1250
	0    -1   -1   0   
$EndComp
Text Label 8150 4800 0    60   ~ 0
RX0_DBG
Text Label 8150 4700 0    60   ~ 0
TX0_DBG
Text Label 8150 4000 0    60   ~ 0
RX1_ESP_5V
Text Label 8150 4100 0    60   ~ 0
TX1_ESP_5V
Text Label 4600 4700 0    60   ~ 0
RX2_485
Text Label 4600 4800 0    60   ~ 0
TX2_485
$Comp
L SP3485CP U2
U 1 1 560F77A4
P 1600 4000
F 0 "U2" H 1300 4350 50  0000 L CNN
F 1 "MAX485" H 1700 4350 50  0000 L CNN
F 2 "SOIC_Packages:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 1600 4000 50  0000 C CIN
F 3 "" H 1600 4000 60  0000 C CNN
	1    1600 4000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 560F8808
P 1600 4500
F 0 "#PWR010" H 1600 4250 50  0001 C CNN
F 1 "GND" H 1600 4350 50  0000 C CNN
F 2 "" H 1600 4500 60  0000 C CNN
F 3 "" H 1600 4500 60  0000 C CNN
	1    1600 4500
	1    0    0    -1  
$EndComp
Text Label 1400 3550 0    60   ~ 0
5V
Text Label 700  4200 0    60   ~ 0
TX2_485
Text Label 700  3800 0    60   ~ 0
RX2_485
$Comp
L R R4
U 1 1 560FA726
P 2200 3700
F 0 "R4" V 2280 3700 50  0000 C CNN
F 1 "680" V 2200 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2130 3700 30  0001 C CNN
F 3 "" H 2200 3700 30  0000 C CNN
	1    2200 3700
	-1   0    0    1   
$EndComp
$Comp
L R R5
U 1 1 560FA7AB
P 2200 4300
F 0 "R5" V 2280 4300 50  0000 C CNN
F 1 "680" V 2200 4300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2130 4300 30  0001 C CNN
F 3 "" H 2200 4300 30  0000 C CNN
	1    2200 4300
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X02 P5
U 1 1 560FADCA
P 2500 4000
F 0 "P5" H 2500 4150 50  0000 C CNN
F 1 "B A" V 2600 4000 50  0000 C CNN
F 2 "w_conn_screw:mstba_2,5%2f2-g-5,08" H 2500 4000 60  0001 C CNN
F 3 "" H 2500 4000 60  0000 C CNN
	1    2500 4000
	1    0    0    1   
$EndComp
Text Label 2050 3900 0    60   ~ 0
A
Text Label 2050 4100 0    60   ~ 0
B
Text Label 8000 5400 0    60   ~ 0
TX_EN_485
Text Label 700  4000 0    60   ~ 0
TX_EN_485
$Comp
L GND #PWR011
U 1 1 5610172D
P 3100 7600
F 0 "#PWR011" H 3100 7350 50  0001 C CNN
F 1 "GND" H 3100 7450 50  0000 C CNN
F 2 "" H 3100 7600 60  0000 C CNN
F 3 "" H 3100 7600 60  0000 C CNN
	1    3100 7600
	1    0    0    -1  
$EndComp
Text Label 3100 7400 0    60   ~ 0
TX0_DBG
Text Label 3100 7500 0    60   ~ 0
RX0_DBG
Text Notes 700  3200 0    60   ~ 0
Power Supply\n
$Comp
L GND #PWR012
U 1 1 56101B56
P 1350 5550
F 0 "#PWR012" H 1350 5300 50  0001 C CNN
F 1 "GND" H 1350 5400 50  0000 C CNN
F 2 "" H 1350 5550 60  0000 C CNN
F 3 "" H 1350 5550 60  0000 C CNN
	1    1350 5550
	1    0    0    -1  
$EndComp
Text Label 700  5400 0    60   ~ 0
5V
$Comp
L R R1
U 1 1 56101DE3
P 900 5600
F 0 "R1" V 980 5600 50  0000 C CNN
F 1 "10K" V 900 5600 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 830 5600 30  0001 C CNN
F 3 "" H 900 5600 30  0000 C CNN
	1    900  5600
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 56101E57
P 1100 5600
F 0 "R2" V 1180 5600 50  0000 C CNN
F 1 "10K" V 1100 5600 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1030 5600 30  0001 C CNN
F 3 "" H 1100 5600 30  0000 C CNN
	1    1100 5600
	1    0    0    -1  
$EndComp
$Comp
L DS3231N U4
U 1 1 5610291F
P 2350 5750
F 0 "U4" H 2650 6300 60  0000 C CNN
F 1 "DS3231N" H 2050 5350 60  0000 C CNN
F 2 "SMD_Packages:SO-16-W" H 2350 5750 60  0001 C CNN
F 3 "" H 2350 5750 60  0000 C CNN
	1    2350 5750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 561038C4
P 3150 6700
F 0 "#PWR013" H 3150 6450 50  0001 C CNN
F 1 "GND" H 3150 6550 50  0000 C CNN
F 2 "" H 3150 6700 60  0000 C CNN
F 3 "" H 3150 6700 60  0000 C CNN
	1    3150 6700
	1    0    0    -1  
$EndComp
Text Label 700  5800 0    60   ~ 0
SDA
Text Label 700  5950 0    60   ~ 0
SCL
Text Label 8150 3800 0    60   ~ 0
SCL
Text Label 8150 3900 0    60   ~ 0
SDA
Text Label 1650 4950 0    60   ~ 0
Vbat
$Comp
L CONN_01X02 P2
U 1 1 56106E1A
P 1350 5000
F 0 "P2" H 1350 5150 50  0000 C CNN
F 1 "CR2032" V 1450 5000 50  0000 C CNN
F 2 "terminal:CR2032_noOUTline" H 1350 5000 60  0001 C CNN
F 3 "" H 1350 5000 60  0000 C CNN
	1    1350 5000
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 56107518
P 1600 5050
F 0 "#PWR014" H 1600 4800 50  0001 C CNN
F 1 "GND" H 1600 4900 50  0000 C CNN
F 2 "" H 1600 5050 60  0000 C CNN
F 3 "" H 1600 5050 60  0000 C CNN
	1    1600 5050
	1    0    0    -1  
$EndComp
NoConn ~ 3250 5700
NoConn ~ 3250 5550
NoConn ~ 3250 5400
Text Notes 700  6850 0    60   ~ 0
RTC
Text Notes 700  4650 0    60   ~ 0
Modus
NoConn ~ 5150 6300
Text Label 14750 1150 0    60   ~ 0
RX1_ESP_5V
Text Label 14650 1550 0    60   ~ 0
TX1_ESP_5V
Wire Wire Line
	1050 900  1100 900 
Wire Wire Line
	1400 900  1600 900 
Wire Wire Line
	1900 1250 1900 1200
Wire Wire Line
	2200 900  2350 900 
Wire Wire Line
	2350 900  2350 950 
Wire Wire Line
	1200 1600 1000 1600
Wire Wire Line
	1800 1600 2000 1600
Wire Wire Line
	1500 1900 1500 2000
Wire Wire Line
	2400 2050 2250 2050
Wire Wire Line
	1050 1250 2350 1250
Connection ~ 1900 1250
Wire Wire Line
	2300 1300 2300 1250
Connection ~ 2300 1250
Wire Wire Line
	1900 1600 1900 1650
Connection ~ 1900 1600
Wire Wire Line
	1500 1950 1900 1950
Connection ~ 1500 1950
Wire Wire Line
	4900 1450 4900 1400
Wire Wire Line
	4650 1400 5150 1400
Wire Wire Line
	4900 1650 4900 1700
Wire Wire Line
	4650 1700 5150 1700
Connection ~ 4900 1700
Connection ~ 4900 1400
Wire Wire Line
	4450 1400 4450 1700
Wire Wire Line
	4450 1550 4300 1550
Wire Wire Line
	4300 1550 4300 1650
Connection ~ 4450 1550
Wire Wire Line
	6100 800  6100 750 
Wire Wire Line
	5950 750  6600 750 
Wire Wire Line
	6300 750  6300 800 
Wire Wire Line
	6200 800  6200 750 
Connection ~ 6200 750 
Connection ~ 6100 750 
Wire Wire Line
	6600 750  6600 800 
Connection ~ 6300 750 
Wire Wire Line
	6300 6600 6300 6700
Wire Wire Line
	6300 6700 6700 6700
Wire Wire Line
	6600 6700 6600 6600
Wire Wire Line
	6400 6600 6400 6700
Connection ~ 6400 6700
Wire Wire Line
	6500 6600 6500 6700
Connection ~ 6500 6700
Wire Wire Line
	6450 6700 6450 6800
Connection ~ 6450 6700
Wire Wire Line
	1800 7300 2050 7300
Wire Wire Line
	1800 7400 2050 7400
Wire Wire Line
	1800 7500 2050 7500
Wire Wire Line
	1300 7300 800  7300
Wire Wire Line
	1300 7400 800  7400
Wire Wire Line
	1300 7500 1050 7500
Wire Wire Line
	4350 1100 5150 1100
Wire Wire Line
	5100 950  5100 1100
Connection ~ 5100 1100
Wire Wire Line
	7750 2100 8100 2100
Wire Wire Line
	7750 2200 8100 2200
Wire Wire Line
	7750 2300 8100 2300
Wire Wire Line
	1500 950  1500 900 
Connection ~ 1500 900 
Wire Wire Line
	4000 7400 4250 7400
Wire Wire Line
	4000 7500 4250 7500
Wire Wire Line
	1000 2350 1950 2350
Wire Wire Line
	1950 2350 1950 2450
Wire Wire Line
	1700 2350 1700 2450
Connection ~ 1700 2350
Wire Wire Line
	1450 2350 1450 2450
Connection ~ 1450 2350
Wire Wire Line
	1200 2350 1200 2450
Connection ~ 1200 2350
Wire Wire Line
	1200 2750 1200 2850
Wire Wire Line
	1200 2850 1950 2850
Wire Wire Line
	1950 2850 1950 2750
Wire Wire Line
	1450 2850 1450 2750
Connection ~ 1450 2850
Wire Wire Line
	1700 2750 1700 2850
Connection ~ 1700 2850
Wire Wire Line
	1600 2850 1600 2900
Connection ~ 1600 2850
Wire Wire Line
	4950 1000 5100 1000
Wire Wire Line
	4650 800  4850 800 
Wire Wire Line
	4850 800  4850 1100
Connection ~ 4850 1100
Wire Wire Line
	4150 800  4350 800 
Wire Wire Line
	4150 1250 4900 1250
Wire Wire Line
	4900 1250 4900 1100
Connection ~ 4900 1100
Wire Wire Line
	3700 1250 3950 1250
Wire Wire Line
	3700 1100 3700 1250
Wire Wire Line
	7750 4700 8150 4700
Wire Wire Line
	7750 4800 8150 4800
Wire Wire Line
	7750 4100 8150 4100
Wire Wire Line
	7750 4000 8150 4000
Wire Wire Line
	5150 4700 4600 4700
Wire Wire Line
	4600 4800 5150 4800
Wire Wire Line
	1200 3900 1200 4100
Wire Wire Line
	700  4000 1200 4000
Connection ~ 1200 4000
Wire Wire Line
	1200 3800 700  3800
Wire Wire Line
	700  4200 1200 4200
Wire Wire Line
	2000 3900 2300 3900
Wire Wire Line
	2000 4100 2300 4100
Wire Wire Line
	1600 3600 1600 3550
Wire Wire Line
	1400 3550 2200 3550
Wire Wire Line
	1600 4400 1600 4500
Wire Wire Line
	2200 4450 1600 4450
Connection ~ 1600 4450
Wire Wire Line
	2200 4150 2200 4100
Connection ~ 2200 4100
Wire Wire Line
	2200 3850 2200 3900
Connection ~ 2200 3900
Connection ~ 1600 3550
Wire Wire Line
	2300 3900 2300 3950
Wire Wire Line
	2300 4100 2300 4050
Wire Wire Line
	2950 7400 3100 7400
Wire Wire Line
	3100 7500 2950 7500
Wire Wire Line
	3100 7600 2950 7600
Wire Notes Line
	650  600  650  3250
Wire Notes Line
	650  3250 2650 3250
Wire Notes Line
	2650 600  650  600 
Wire Wire Line
	700  5400 1450 5400
Wire Wire Line
	700  5800 1450 5800
Wire Wire Line
	700  5950 1450 5950
Wire Wire Line
	900  5450 900  5400
Connection ~ 900  5400
Wire Wire Line
	1100 5450 1100 5400
Connection ~ 1100 5400
Wire Wire Line
	900  5750 900  5800
Connection ~ 900  5800
Wire Wire Line
	1100 5750 1100 5950
Connection ~ 1100 5950
Wire Wire Line
	2550 6600 2550 6700
Wire Wire Line
	3350 6700 1250 6700
Wire Wire Line
	2750 6700 2750 6600
Wire Wire Line
	2650 6600 2650 6700
Connection ~ 2650 6700
Wire Wire Line
	3250 5800 3350 5800
Wire Wire Line
	3350 5800 3350 6700
Wire Wire Line
	3350 5900 3250 5900
Connection ~ 2750 6700
Connection ~ 3350 5900
Wire Wire Line
	3250 6000 3350 6000
Connection ~ 3350 6000
Wire Wire Line
	3250 6100 3350 6100
Connection ~ 3350 6100
Wire Wire Line
	3250 6200 3350 6200
Connection ~ 3350 6200
Wire Wire Line
	7750 3800 8150 3800
Wire Wire Line
	7750 3900 8150 3900
Wire Wire Line
	1550 4950 2100 4950
Wire Wire Line
	1450 5550 1350 5550
Wire Wire Line
	1550 5050 1600 5050
Wire Notes Line
	650  4850 650  6900
Wire Notes Line
	650  6900 3400 6900
Wire Notes Line
	3400 6900 3400 4850
Wire Notes Line
	3400 4850 650  4850
Wire Notes Line
	650  3450 3150 3450
Wire Notes Line
	3150 3450 3150 4700
Wire Notes Line
	3150 4700 650  4700
Wire Notes Line
	650  4700 650  3450
Wire Notes Line
	2650 3250 2650 600 
Connection ~ 1900 2850
NoConn ~ 5150 6000
NoConn ~ 5150 5900
NoConn ~ 5150 5800
NoConn ~ 5150 5700
NoConn ~ 5150 5600
NoConn ~ 5150 5400
NoConn ~ 5150 5300
NoConn ~ 5150 5200
NoConn ~ 5150 5100
NoConn ~ 5150 4500
NoConn ~ 5150 4400
NoConn ~ 5150 4300
NoConn ~ 5150 4200
NoConn ~ 5150 4100
NoConn ~ 5150 4000
NoConn ~ 5150 3900
NoConn ~ 5150 3800
NoConn ~ 5150 3600
NoConn ~ 5150 3500
NoConn ~ 5150 3400
NoConn ~ 5150 3300
NoConn ~ 5150 3200
NoConn ~ 5150 3100
NoConn ~ 5150 3000
NoConn ~ 5150 2900
NoConn ~ 5150 2700
NoConn ~ 5150 2600
NoConn ~ 5150 2500
NoConn ~ 5150 2400
NoConn ~ 5150 2200
NoConn ~ 5150 2100
NoConn ~ 5150 2000
NoConn ~ 7750 1100
NoConn ~ 7750 1200
NoConn ~ 7750 1300
NoConn ~ 7750 1400
NoConn ~ 7750 1500
NoConn ~ 7750 1600
NoConn ~ 7750 1700
NoConn ~ 7750 1800
Wire Wire Line
	7750 2000 8100 2000
Text Label 8100 2000 0    60   ~ 0
SD_CS_5V
NoConn ~ 7750 2400
NoConn ~ 7750 2600
NoConn ~ 7750 2500
NoConn ~ 7750 2700
NoConn ~ 7750 2900
NoConn ~ 7750 3000
NoConn ~ 7750 3100
NoConn ~ 7750 3200
NoConn ~ 7750 3300
NoConn ~ 7750 3400
NoConn ~ 7750 3500
NoConn ~ 7750 3600
NoConn ~ 7750 4200
NoConn ~ 7750 4300
NoConn ~ 7750 4400
NoConn ~ 7750 4500
NoConn ~ 7750 4900
NoConn ~ 7750 5000
NoConn ~ 7750 5200
NoConn ~ 7750 5300
NoConn ~ 7750 6200
NoConn ~ 7750 6300
Text Label 2000 1600 0    60   ~ 0
3V3
$Comp
L PWR_FLAG #FLG015
U 1 1 561A8D10
P 2350 4950
F 0 "#FLG015" H 2350 5045 50  0001 C CNN
F 1 "PWR_FLAG" H 2350 5130 50  0000 C CNN
F 2 "" H 2350 4950 60  0000 C CNN
F 3 "" H 2350 4950 60  0000 C CNN
	1    2350 4950
	-1   0    0    1   
$EndComp
Wire Wire Line
	2350 4950 2350 4900
Wire Wire Line
	2350 4900 2000 4900
Wire Wire Line
	2000 4900 2000 4950
Connection ~ 2000 4950
Connection ~ 1500 1250
NoConn ~ 5150 2300
Wire Wire Line
	3600 1100 3750 1100
Connection ~ 3700 1100
$Comp
L CONN_01X02 P1
U 1 1 5616BC0A
P 850 950
F 0 "P1" H 850 1100 50  0000 C CNN
F 1 "PWR IN" V 950 950 50  0000 C CNN
F 2 "Connect:BARREL_JACK" H 850 950 60  0001 C CNN
F 3 "" H 850 950 60  0000 C CNN
	1    850  950 
	-1   0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG016
U 1 1 5617E78F
P 4100 7350
F 0 "#FLG016" H 4100 7445 50  0001 C CNN
F 1 "PWR_FLAG" H 4100 7550 50  0000 C CNN
F 2 "" H 4100 7350 60  0000 C CNN
F 3 "" H 4100 7350 60  0000 C CNN
	1    4100 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 7350 4100 7400
Connection ~ 4100 7400
$Comp
L C C2
U 1 1 561AA456
P 1250 6250
F 0 "C2" H 1275 6350 50  0000 L CNN
F 1 "10nF" H 1275 6150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1288 6100 30  0001 C CNN
F 3 "" H 1250 6250 60  0000 C CNN
	1    1250 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 6100 1250 5400
Connection ~ 1250 5400
Wire Wire Line
	1250 6700 1250 6400
Connection ~ 2550 6700
$Comp
L LED D3
U 1 1 561CB621
P 2900 4100
F 0 "D3" H 2900 4200 50  0000 C CNN
F 1 "MB" H 2900 4000 50  0000 C CNN
F 2 "LEDs:LED-0805" H 2900 4100 60  0001 C CNN
F 3 "" H 2900 4100 60  0000 C CNN
	1    2900 4100
	0    -1   -1   0   
$EndComp
$Comp
L R R7
U 1 1 561CD993
P 2900 3700
F 0 "R7" V 2980 3700 50  0000 C CNN
F 1 "10K" V 2900 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2830 3700 30  0001 C CNN
F 3 "" H 2900 3700 30  0000 C CNN
	1    2900 3700
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR017
U 1 1 561CFC4E
P 2900 4450
F 0 "#PWR017" H 2900 4200 50  0001 C CNN
F 1 "GND" H 2900 4300 50  0000 C CNN
F 2 "" H 2900 4450 60  0000 C CNN
F 3 "" H 2900 4450 60  0000 C CNN
	1    2900 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 3550 2650 3550
Wire Wire Line
	2900 3850 2900 3900
Wire Wire Line
	2900 4300 2900 4450
Text Label 4600 4900 0    60   ~ 0
MB_ACT
Text Label 2650 3550 0    60   ~ 0
MB_ACT
Text Label 13200 1550 0    60   ~ 0
TX1_ESP_3V3
Wire Wire Line
	1050 1250 1050 1000
Wire Wire Line
	2400 2800 2400 2850
Wire Wire Line
	2400 2350 2400 2400
$Comp
L PWR_FLAG #FLG018
U 1 1 5622FFCC
P 4100 7700
F 0 "#FLG018" H 4100 7795 50  0001 C CNN
F 1 "PWR_FLAG" H 4100 7880 50  0000 C CNN
F 2 "" H 4100 7700 60  0000 C CNN
F 3 "" H 4100 7700 60  0000 C CNN
	1    4100 7700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4100 7700 4100 7500
Connection ~ 4100 7500
$Comp
L ESP-12E U6
U 1 1 562B6140
P 12050 1750
F 0 "U6" H 12050 1650 50  0000 C CNN
F 1 "ESP-12E" H 12050 1850 50  0000 C CNN
F 2 "ESP8266:ESP-12E" H 12050 1750 50  0001 C CNN
F 3 "" H 12050 1750 50  0001 C CNN
	1    12050 1750
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 562F5BE6
P 10300 1300
F 0 "R9" V 10380 1300 50  0000 C CNN
F 1 "10K" V 10300 1300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 10230 1300 30  0001 C CNN
F 3 "" H 10300 1300 30  0000 C CNN
	1    10300 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	10300 1450 11150 1450
Wire Wire Line
	10000 1150 10300 1150
Text Label 10000 1150 0    60   ~ 0
3V3
Wire Wire Line
	10150 2150 11150 2150
Wire Wire Line
	10150 1150 10150 2150
Connection ~ 10150 1150
$Comp
L R R10
U 1 1 562F773B
P 10650 1650
F 0 "R10" V 10730 1650 50  0000 C CNN
F 1 "100" V 10650 1650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 10580 1650 30  0001 C CNN
F 3 "" H 10650 1650 30  0000 C CNN
	1    10650 1650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10800 1650 11150 1650
Wire Wire Line
	10500 1650 10150 1650
Connection ~ 10150 1650
NoConn ~ 11150 1550
NoConn ~ 11150 1750
NoConn ~ 11150 1850
NoConn ~ 11150 1950
NoConn ~ 11150 2050
NoConn ~ 11800 2650
NoConn ~ 11900 2650
NoConn ~ 12000 2650
NoConn ~ 12100 2650
NoConn ~ 12200 2650
NoConn ~ 12300 2650
NoConn ~ 12950 1750
NoConn ~ 12950 1650
$Comp
L GND #PWR019
U 1 1 562FD365
P 13400 2200
F 0 "#PWR019" H 13400 1950 50  0001 C CNN
F 1 "GND" H 13400 2050 50  0000 C CNN
F 2 "" H 13400 2200 60  0000 C CNN
F 3 "" H 13400 2200 60  0000 C CNN
	1    13400 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	12950 1450 13200 1450
Wire Wire Line
	12950 1550 13750 1550
Wire Notes Line
	650  7100 4450 7100
Wire Notes Line
	4450 7100 4450 8150
Wire Notes Line
	4450 8150 650  8150
Wire Notes Line
	650  8150 650  7100
Text Notes 700  8100 0    60   ~ 0
Misc. Headers
Wire Notes Line
	9850 1000 15250 1000
Wire Notes Line
	15250 1000 15250 2850
Wire Notes Line
	15250 2850 9850 2850
Wire Notes Line
	9850 2850 9850 1000
Text Notes 9900 2800 0    60   ~ 0
ESP-12
NoConn ~ 7750 6000
NoConn ~ 7750 6100
$Comp
L MicroSD J1
U 1 1 56282C10
P 12900 5200
F 0 "J1" H 12700 5900 60  0000 C CNN
F 1 "MicroSD" H 12700 4500 60  0000 C CNN
F 2 "Conn_uSDcard:Conn_uSDcard" H 13000 5350 60  0001 C CNN
F 3 "" H 13000 5350 60  0000 C CNN
	1    12900 5200
	-1   0    0    -1  
$EndComp
NoConn ~ 12700 4600
NoConn ~ 12700 5500
NoConn ~ 12700 5600
NoConn ~ 12700 5700
NoConn ~ 12700 5800
$Comp
L 4050 U5
U 3 1 562841E7
P 11400 5400
F 0 "U5" H 11595 5515 60  0000 C CNN
F 1 "4050" H 11590 5275 60  0000 C CNN
F 2 "SOIC_Packages:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 11400 5400 60  0001 C CNN
F 3 "" H 11400 5400 60  0000 C CNN
	3    11400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	11350 3550 11350 3800
Wire Wire Line
	10050 3550 11350 3550
Wire Wire Line
	11350 4300 11350 4200
Wire Wire Line
	11350 4200 10800 4200
Wire Wire Line
	11350 4000 11350 4100
Wire Wire Line
	11350 4100 11950 4100
Wire Wire Line
	11350 4500 11350 4600
Wire Wire Line
	11350 4600 11950 4600
Wire Wire Line
	11350 5000 11350 5100
Wire Wire Line
	11350 5100 11950 5100
Wire Wire Line
	11350 5500 11350 5600
Wire Wire Line
	11350 5600 11950 5600
Wire Wire Line
	11350 4800 11350 4700
Wire Wire Line
	11350 4700 10800 4700
Wire Wire Line
	11350 5300 11350 5200
Wire Wire Line
	11350 5200 10800 5200
Wire Wire Line
	10800 5200 10800 3550
Connection ~ 10800 3550
Connection ~ 10800 4200
Connection ~ 10800 4700
Wire Wire Line
	11950 4100 11950 5700
Connection ~ 11950 4600
Connection ~ 11950 5100
Connection ~ 11950 5600
$Comp
L GND #PWR020
U 1 1 5628B185
P 11950 5700
F 0 "#PWR020" H 11950 5450 50  0001 C CNN
F 1 "GND" H 11950 5550 50  0000 C CNN
F 2 "" H 11950 5700 60  0000 C CNN
F 3 "" H 11950 5700 60  0000 C CNN
	1    11950 5700
	1    0    0    -1  
$EndComp
Text Label 10050 3550 0    60   ~ 0
3V3
Wire Wire Line
	10950 3900 10100 3900
Wire Wire Line
	10950 4400 10100 4400
Wire Wire Line
	10100 4900 10950 4900
Text Label 10100 3900 0    60   ~ 0
SD_CS_5V
Text Label 10100 4400 0    60   ~ 0
MOSI_5V
Text Label 10100 4900 0    60   ~ 0
SCK_5V
Wire Wire Line
	10950 5400 10450 5400
Wire Wire Line
	10450 5400 10450 4900
Connection ~ 10450 4900
Wire Wire Line
	11850 3900 12550 3900
Wire Wire Line
	12550 3900 12550 5300
Wire Wire Line
	12550 4700 12700 4700
Wire Wire Line
	12700 4800 12450 4800
Wire Wire Line
	12450 4800 12450 4400
Wire Wire Line
	12450 4400 11850 4400
Wire Wire Line
	11850 4900 12100 4900
Wire Wire Line
	12100 4900 12100 5000
Wire Wire Line
	12100 5000 12700 5000
Wire Wire Line
	12700 5100 12050 5100
Wire Wire Line
	12050 5100 12050 5000
Wire Wire Line
	12050 5000 11950 5000
Connection ~ 11950 5000
Wire Wire Line
	12700 5200 12150 5200
NoConn ~ 12700 5300
Wire Wire Line
	12700 4900 12300 4900
Text Label 12300 4900 0    60   ~ 0
3V3
Text Label 12150 5200 0    60   ~ 0
MISO_5V
$Comp
L LED D4
U 1 1 5629141C
P 12250 5700
F 0 "D4" H 12250 5800 50  0000 C CNN
F 1 "SD" H 12250 5600 50  0000 C CNN
F 2 "LEDs:LED-0805" H 12250 5700 60  0001 C CNN
F 3 "" H 12250 5700 60  0000 C CNN
	1    12250 5700
	-1   0    0    1   
$EndComp
$Comp
L R R11
U 1 1 562914F7
P 12550 5450
F 0 "R11" V 12630 5450 50  0000 C CNN
F 1 "100" V 12550 5450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 12480 5450 30  0001 C CNN
F 3 "" H 12550 5450 30  0000 C CNN
	1    12550 5450
	-1   0    0    1   
$EndComp
Wire Wire Line
	11850 5400 12050 5400
Wire Wire Line
	12050 5400 12050 5700
Connection ~ 12550 4700
Text Label 12050 3900 0    60   ~ 0
SD_CS_3V3
Text Label 12050 4400 0    60   ~ 0
MOSI_3V3
Text Label 12150 5000 0    60   ~ 0
SCK_3V3
Wire Wire Line
	12450 5700 12550 5700
Wire Wire Line
	12550 5700 12550 5600
Wire Wire Line
	7750 5400 8000 5400
Wire Wire Line
	4600 4900 5150 4900
Text Notes 10700 -300 0    118  ~ 0
TODO: \nAdd IO pin for hard reset\nAdd open Switch\nLED indicators\noptional: Add temp sensor
$Comp
L CONN_01X04 P6
U 1 1 5673C578
P 2750 7450
F 0 "P6" H 2750 7700 50  0000 C CNN
F 1 "DEBUG" V 2850 7450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 2750 7450 60  0001 C CNN
F 3 "" H 2750 7450 60  0000 C CNN
	1    2750 7450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2950 7300 3100 7300
Text Label 3100 7300 0    60   ~ 0
DTR
$Comp
L C C12
U 1 1 5673F208
P 5100 800
F 0 "C12" H 5125 900 50  0000 L CNN
F 1 "10nF" H 4900 900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 5138 650 30  0001 C CNN
F 3 "" H 5100 800 60  0000 C CNN
	1    5100 800 
	1    0    0    -1  
$EndComp
Connection ~ 5100 1000
Wire Wire Line
	5100 650  4850 650 
Text Label 4850 650  0    60   ~ 0
DTR
NoConn ~ 5150 5000
NoConn ~ 7750 5100
NoConn ~ 7750 5600
NoConn ~ 7750 5700
NoConn ~ 7750 5800
NoConn ~ 7750 5900
NoConn ~ 1800 5100
$Comp
L 4050 U5
U 6 1 5675CBD7
P 11400 3900
F 0 "U5" H 11595 4015 60  0000 C CNN
F 1 "4050" H 11590 3775 60  0000 C CNN
F 2 "SOIC_Packages:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 11400 3900 60  0001 C CNN
F 3 "" H 11400 3900 60  0000 C CNN
	6    11400 3900
	1    0    0    -1  
$EndComp
$Comp
L 4050 U5
U 2 1 56768D43
P 14200 1550
F 0 "U5" H 14395 1665 60  0000 C CNN
F 1 "4050" H 14390 1425 60  0000 C CNN
F 2 "SOIC_Packages:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 14200 1550 60  0001 C CNN
F 3 "" H 14200 1550 60  0000 C CNN
	2    14200 1550
	-1   0    0    1   
$EndComp
$Comp
L SW_PUSH SW2
U 1 1 568BF546
P 10850 1200
F 0 "SW2" H 10850 1200 50  0000 C CNN
F 1 "~R" H 10850 1350 50  0000 C CNN
F 2 "IHE:SW_M6_PTH" H 10850 1200 60  0001 C CNN
F 3 "" H 10850 1200 60  0000 C CNN
	1    10850 1200
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW3
U 1 1 568BFB35
P 13650 1850
F 0 "SW3" H 13650 1850 50  0000 C CNN
F 1 "FU" H 13650 2000 50  0000 C CNN
F 2 "IHE:SW_M6_PTH" H 13650 1850 60  0001 C CNN
F 3 "" H 13650 1850 60  0000 C CNN
	1    13650 1850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 568C0383
P 11300 1200
F 0 "#PWR021" H 11300 950 50  0001 C CNN
F 1 "GND" H 11300 1050 50  0000 C CNN
F 2 "" H 11300 1200 60  0000 C CNN
F 3 "" H 11300 1200 60  0000 C CNN
	1    11300 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 1200 11300 1200
Wire Wire Line
	10550 1200 10550 1450
Connection ~ 10550 1450
Wire Wire Line
	12950 1850 13350 1850
Wire Wire Line
	12950 1950 13200 1950
Text Label 13200 1950 0    60   ~ 0
3V3
Wire Wire Line
	12950 2150 13950 2150
Wire Wire Line
	13400 2150 13400 2200
Wire Wire Line
	12950 2050 13200 2050
Wire Wire Line
	13200 2050 13200 2150
Connection ~ 13200 2150
Wire Wire Line
	13950 2150 13950 1850
Connection ~ 13400 2150
$Comp
L 4050 U5
U 1 1 568C6EE5
P 14000 1150
F 0 "U5" H 14195 1265 60  0000 C CNN
F 1 "4050" H 14190 1025 60  0000 C CNN
F 2 "SOIC_Packages:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 14000 1150 60  0001 C CNN
F 3 "" H 14000 1150 60  0000 C CNN
	1    14000 1150
	1    0    0    1   
$EndComp
$Comp
L 4050 U5
U 5 1 568CAD0F
P 11400 4400
F 0 "U5" H 11595 4515 60  0000 C CNN
F 1 "4050" H 11590 4275 60  0000 C CNN
F 2 "SOIC_Packages:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 11400 4400 60  0001 C CNN
F 3 "" H 11400 4400 60  0000 C CNN
	5    11400 4400
	1    0    0    -1  
$EndComp
$Comp
L 4050 U5
U 4 1 568CADC3
P 11400 4900
F 0 "U5" H 11595 5015 60  0000 C CNN
F 1 "4050" H 11590 4775 60  0000 C CNN
F 2 "SOIC_Packages:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 11400 4900 60  0001 C CNN
F 3 "" H 11400 4900 60  0000 C CNN
	4    11400 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	13550 1150 13200 1150
Wire Wire Line
	13200 1150 13200 1450
Wire Wire Line
	14450 1150 14750 1150
$Comp
L CONN_01X02 P3
U 1 1 568D73FF
P 1350 8450
F 0 "P3" H 1350 8600 50  0000 C CNN
F 1 "SW" V 1450 8450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 1350 8450 60  0001 C CNN
F 3 "" H 1350 8450 60  0000 C CNN
	1    1350 8450
	-1   0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 568DA2FF
P 1850 8400
F 0 "R3" V 1930 8400 50  0000 C CNN
F 1 "10K" V 1850 8400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1780 8400 30  0001 C CNN
F 3 "" H 1850 8400 30  0000 C CNN
	1    1850 8400
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 8400 1550 8400
$Comp
L GND #PWR022
U 1 1 568DAA93
P 1650 8550
F 0 "#PWR022" H 1650 8300 50  0001 C CNN
F 1 "GND" H 1650 8400 50  0000 C CNN
F 2 "" H 1650 8550 60  0000 C CNN
F 3 "" H 1650 8550 60  0000 C CNN
	1    1650 8550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 8500 1650 8500
Wire Wire Line
	1650 8500 1650 8550
Wire Wire Line
	2000 8400 2250 8400
Text Label 2250 8400 0    60   ~ 0
OPEN_DETECT
Wire Wire Line
	5150 6100 4400 6100
Text Label 4400 6100 0    60   ~ 0
OPEN_DETECT
Wire Wire Line
	6400 800  6400 750 
Connection ~ 6400 750 
Wire Wire Line
	6700 6700 6700 6600
Connection ~ 6600 6700
Connection ~ 3150 6700
$EndSCHEMATC
