Energy Monitoring System
=======================
This device finds the energy consumption from motor inverters used in factories via modbus and post the data on the internet.

*Second Attempt: Using ESP8266 to upload data to the server.

*First Attempt: Used arduino with modbus library and data was being sent via serial to a Desktop PC, later the desktop pc was replaced by a raspberry pi
