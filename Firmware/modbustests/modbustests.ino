#include <SimpleModbusMaster.h>
#include <SoftwareSerial.h>
/*
   The example will use PACKET1 to read a register from address 0 (the adc ch0 value)
   from the arduino slave (id=1). It will then use this value to adjust the brightness
   of an led on pin 9 using PWM.
   It will then use packet2 to write a register (its own adc ch0 value) to address 1
   on the arduino slave (id=1) adjusting the brightness of an led on pin 9 using PWM.

 Recommended Modbus slave:
 *  diagslave http://www.modbusdriver.com/diagslave.html
 *
 *  In a Linux box, run
 *  "./diagslave /dev/ttyUSB0 -b 19200 -d 8 -s 2 -p none -m rtu -a 1"
 *  This is:
 *    serial port /dev/ttyUSB0 at 19200 baud 8N2
 *    RTU mode and address @1


 */

//////////////////// Port information ///////////////////
#define baud 9600 //modbus baud
#define timeout 500
//#define timeout 1000
#define polling 1000 // the scan rate
//#define polling 200 // the scan rate
#define retry_count 10

// used to toggle the receive/transmit pin on the driver
#define TxEnablePin 9

#define LED 13

// The total amount of available memory on the master to store data
#define TOTAL_NO_OF_REGISTERS 170

char buff[150];

// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS is automatically updated.
enum
{
  PACKET1,
  PACKET2,
  PACKET3,
  PACKET4,
  PACKET5,
  PACKET6,
  PACKET7,
  PACKET8,
  PACKET9,
  PACKET10,
  PACKET11,
  PACKET12,
  PACKET13,
  PACKET14,
  PACKET15,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];
long lastPeriod = 0;
SoftwareSerial debug(2, 3); //rx tx
int i, j;

void setup()
{
  //modbus_construct(packet,slaveid,command,reg address,datalen,local reg storage index)
  // modbus_construct(&packets[PACKET1], 2, READ_HOLDING_REGISTERS, 0x3000, 8, 0);//invt 1-7
  //Data:op freq,ref freq,dc bus voltage,op voltage,op current,rotation speed,power,torque
  // modbus_construct(&packets[PACKET2], 8, READ_HOLDING_REGISTERS, 0x3211, 8, 8);//sb200 8- 15 (16 back up pump)
  //Data:
  //modbus_construct(&packets[PACKET1], 17, READ_HOLDING_REGISTERS, 0x40000, 7, 0);//ABB 17-21 (16 back up pump)
  //Data:
  //------INVT----------------------
  modbus_construct(&packets[PACKET1], 1, READ_HOLDING_REGISTERS, 0x3000, 8, 0);//invt 1-7
  modbus_construct(&packets[PACKET2], 2, READ_HOLDING_REGISTERS, 0x3000, 8, 8);//invt 1-7
  modbus_construct(&packets[PACKET3], 3, READ_HOLDING_REGISTERS, 0x3000, 8, 16);//invt 1-7
  modbus_construct(&packets[PACKET4], 4, READ_HOLDING_REGISTERS, 0x3000, 8, 24);//invt 1-7
  modbus_construct(&packets[PACKET5], 5, READ_HOLDING_REGISTERS, 0x3000, 8, 32);//invt 1-7
  modbus_construct(&packets[PACKET6], 6, READ_HOLDING_REGISTERS, 0x3000, 8, 40);//invt 1-7
  modbus_construct(&packets[PACKET7], 7, READ_HOLDING_REGISTERS, 0x3000, 8, 48);//invt 1-7
  //  ----------------------
  //------SB-200------------
  modbus_construct(&packets[PACKET8], 8, READ_HOLDING_REGISTERS, 0x3211, 8, 56);
  modbus_construct(&packets[PACKET9], 9, READ_HOLDING_REGISTERS, 0x3211, 8, 64);
  modbus_construct(&packets[PACKET10], 10, READ_HOLDING_REGISTERS, 0x3211, 8, 72);
  modbus_construct(&packets[PACKET11], 11, READ_HOLDING_REGISTERS, 0x3211, 8, 80);
  modbus_construct(&packets[PACKET12], 12, READ_HOLDING_REGISTERS, 0x3211, 8, 88);
  modbus_construct(&packets[PACKET13], 13, READ_HOLDING_REGISTERS, 0x3211, 8, 96);
  modbus_construct(&packets[PACKET14], 14, READ_HOLDING_REGISTERS, 0x3211, 8, 104);
  modbus_construct(&packets[PACKET15], 15, READ_HOLDING_REGISTERS, 0x3211, 8, 112);
  //-----------------------
  
  // Initialize the Modbus Finite State Machine
  modbus_configure(&Serial1, baud, SERIAL_8N1, timeout, polling, retry_count, TxEnablePin, packets, TOTAL_NO_OF_PACKETS, regs);

  pinMode(LED, OUTPUT);
  debug.begin(9600);

}

void loop()
{
  modbus_update();
  //INVT
  for (i = 0; i <=7; i++) {
    j = i * 8;
    //sprintf(buff, "\n", packets[i].id,',', regs[j], ',', regs[j + 1],',', regs[j + 2],',', regs[j + 3],',', regs[j + 4],',', regs[j + 5],',', regs[j + 6],',', regs[j + 7]);
    //debug.print(buff);
    //debug.println(packets[i].id + ','+regs[j]+',' + regs[j + 1] + ',', regs[j + 2] + ',' + regs[j + 3] + ',' + regs[j + 4] + ',' + regs[j + 5] + ',' + regs[j + 6] + ',' + regs[j + 7]);
  
    debug.print(packets[i].id);
    debug.print(",");
    debug.print(regs[j]);
    debug.print(",");
    debug.print(regs[j+1]);
    debug.print(",");
    debug.print(regs[j+2]);
    debug.print(",");
    debug.print(regs[j+3]);
    debug.print(",");
    debug.print(regs[j+4]);
    debug.print(",");
    debug.print(regs[j+5]);
    debug.print(",");
    debug.print(regs[j+6]);
    debug.print(",");
    debug.println(regs[j+7]);
    //delay(100);
  }
  modbus_update();
  //SB-200
  for (i = 8; i <=15; i++) {
    j = i * 8;
    //sprintf(buff, "\n", packets[i].id,',', regs[j], ',', regs[j + 1],',', regs[j + 2],',', regs[j + 3],',', regs[j + 4],',', regs[j + 5],',', regs[j + 6],',', regs[j + 7]);
    //debug.print(buff);
    //debug.println(packets[i].id + ','+regs[j]+',' + regs[j + 1] + ',', regs[j + 2] + ',' + regs[j + 3] + ',' + regs[j + 4] + ',' + regs[j + 5] + ',' + regs[j + 6] + ',' + regs[j + 7]);
    debug.print(packets[i].id);
    debug.print(",");
    debug.print(regs[j]);
    debug.print(",");
    debug.print(regs[j+1]);
    debug.print(",");
    debug.print(regs[j+2]);
    debug.print(",");
    debug.print(regs[j+3]);
    debug.print(",");
    debug.print(regs[j+4]);
    debug.print(",");
    debug.print(regs[j+5]);
    debug.print(",");
    debug.print(regs[j+6]);
    debug.print(",");
    debug.println(regs[j+7]);
   // delay(100);
  }
  /*
    for (int i = 0; i < 8 ; i++) {
      debug.print(regs[i]);
      debug.print(',');
    }
    debug.println(" ");
    debug.println("--");
    debug.println(packets[0].failed_requests);
    //debug.println(packets[1].failed_requests);
    //    debug.println(packets[1].exception_errors);
    //    exception_errors
    debug.println("--");
    delay(200);
    ------------------------*/
  /*
  debug.println(" ");
  debug.println(" ");
  debug.println(" ");
  debug.println("Exception Errors: "+packets[i].exception_errors);
  delay(100);
  debug.println("Failed requests: "+packets[i].failed_requests);
  delay(100);

  delay(100);
  debug.println("Requests: "+packets[i].requests);
  delay(100);
  debug.println("Successful request: "+packets[i].successful_requests);

  //debug.println(regs[i]);
  }
  delay(500);
  //TODO: construct string and sent it out to serial
  /*for(i=0; i<=30; i++){

  j=i*4;

  sprintf(buff, "\n",packets[i].id," ",regs[j]," ",regs[j+1]," ",regs[j+2]," ",regs[j+2]);
  debug.println(buff);

  } */
}

// update the array with the counter data

/*regs[2] = packet1->requests;
regs[3] = packet1->successful_requests;
regs[4] = packet1->failed_requests;
regs[5] = packet1->exception_errors;
regs[6] = packet2->requests;
regs[7] = packet2->successful_requests;
regs[8] = packet2->failed_requests;
regs[9] = packet2->exception_errors;
Serial.println(packet1->requests);
  Serial.println(packet1->successful_requests);
  Serial.println(packet1->failed_requests);
  Serial.println(packet1->exception_errors);
*/



/*modbus_construct(&packets[PACKET2], 2, READ_HOLDING_REGISTERS, 0, 1, 4);
modbus_construct(&packets[PACKET3], 3, READ_HOLDING_REGISTERS, 0, 1, 8);
modbus_construct(&packets[PACKET4], 4, READ_HOLDING_REGISTERS, 0, 1, 12);
modbus_construct(&packets[PACKET5], 5, READ_HOLDING_REGISTERS, 0, 1, 16);
modbus_construct(&packets[PACKET6], 6, READ_HOLDING_REGISTERS, 0, 1, 20);
modbus_construct(&packets[PACKET7], 7, READ_HOLDING_REGISTERS, 0, 1, 24);
modbus_construct(&packets[PACKET8], 8, READ_HOLDING_REGISTERS, 0, 1, 28);
modbus_construct(&packets[PACKET9], 9, READ_HOLDING_REGISTERS, 0, 1, 32);
modbus_construct(&packets[PACKET10], 10, READ_HOLDING_REGISTERS, 0, 1, 36);
modbus_construct(&packets[PACKET11], 11, READ_HOLDING_REGISTERS, 0, 1, 40);
modbus_construct(&packets[PACKET12], 12, READ_HOLDING_REGISTERS, 0, 1, 44);
modbus_construct(&packets[PACKET13], 13, READ_HOLDING_REGISTERS, 0, 1, 48);
modbus_construct(&packets[PACKET14], 14, READ_HOLDING_REGISTERS, 0, 1, 52);
modbus_construct(&packets[PACKET15], 15, READ_HOLDING_REGISTERS, 0, 1, 56);
modbus_construct(&packets[PACKET16], 16, READ_HOLDING_REGISTERS, 0, 1, 60);
modbus_construct(&packets[PACKET17], 17, READ_HOLDING_REGISTERS, 0, 1, 64);
modbus_construct(&packets[PACKET18], 18, READ_HOLDING_REGISTERS, 0, 1, 68);
modbus_construct(&packets[PACKET19], 19, READ_HOLDING_REGISTERS, 0, 1, 72);
modbus_construct(&packets[PACKET20], 20, READ_HOLDING_REGISTERS, 0, 1, 76);
modbus_construct(&packets[PACKET21], 21, READ_HOLDING_REGISTERS, 0, 1, 80);
modbus_construct(&packets[PACKET22], 22, READ_HOLDING_REGISTERS, 0, 1, 84);
modbus_construct(&packets[PACKET23], 23, READ_HOLDING_REGISTERS, 0, 1, 88);
modbus_construct(&packets[PACKET24], 24, READ_HOLDING_REGISTERS, 0, 1, 92);
modbus_construct(&packets[PACKET25], 25, READ_HOLDING_REGISTERS, 0, 1, 96);
modbus_construct(&packets[PACKET26], 26, READ_HOLDING_REGISTERS, 1, 1, 100);
modbus_construct(&packets[PACKET27], 27, READ_HOLDING_REGISTERS, 0, 1, 104);
modbus_construct(&packets[PACKET28], 28, READ_HOLDING_REGISTERS, 0, 1, 108);
modbus_construct(&packets[PACKET29], 29, READ_HOLDING_REGISTERS, 0, 1, 112);
modbus_construct(&packets[PACKET30], 30, READ_HOLDING_REGISTERS, 0, 1, 114);



// Initialize each packet
//modbus_construct(packet,slaveid,command,reg address,datalen,local reg stor index)
//modbus_construct(&packets[PACKET1], 1, READ_COIL_STATUS, 0, 100, 0);

//   modbus_construct(&packets[PACKET1], 5, READ_HOLDING_REGISTERS, 0x3000, 8, 0);//invt 1-7
//Data:
//  modbus_construct(&packets[PACKET2], 8, READ_HOLDING_REGISTERS, 0x3211, 8, 0);//sb200 8- 15 (16 back up pump)
//Data:
//modbus_construct(&packets[PACKET2], 17, READ_HOLDING_REGISTERS, 0x40005, 8, 0);//ABB 17-21 (16 back up pump)
//Data:

//op frep
//set freq
//bus voltage
//output voltage
//current
//speed
//power
//torque

*/
