//Date:20-8-15
//send modbus data via esp to webserver

//Hardware
//serial interfaces
//modbus  Harware serial
//debug(2,3)Software serial 
//4 txEn 
//6 LED status of modbus activity
//esp8266(7,8) Software serial
//reset button
//Power LED

//In Mega
//RTC     I2C
//SD card SPI

#include <SimpleModbusMaster.h>
#include <SoftwareSerial.h>
//#include <Wire.h>
//#include <SD.h>

#define baud 9600 //modbus baud
#define timeout 500
//#define timeout 1000
#define polling 1000 // the scan rate
//#define polling 200 // the scan rate
#define retry_count 10 //tweek this

// used to toggle the receive/transmit pin on the driver
#define TxEnablePin 4

#define LED 6

// The total amount of available memory on the master to store data
#define TOTAL_NO_OF_REGISTERS 170
#define debugging true

SoftwareSerial debug(2, 3); //rx tx
SoftwareSerial esp8266(7,8); //rx tx

char buff[150];

// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS is automatically updated.
enum
{
  PACKET1,
  PACKET2,
  PACKET3,
  PACKET4,
  PACKET5,
  PACKET6,
  PACKET7,
  PACKET8,
  PACKET9,
  PACKET10,
  PACKET11,
  PACKET12,
  PACKET13,
  PACKET14,
  PACKET15,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];
long lastPeriod = 0;
int i, j;

void setup()
{
  //modbus_construct(packet,slaveid,command,reg address,datalen,local reg storage index)
  // modbus_construct(&packets[PACKET1], 2, READ_HOLDING_REGISTERS, 0x3000, 8, 0);//invt 1-7
  //Data:op freq,ref freq,dc bus voltage,op voltage,op current,rotation speed,power,torque
  // modbus_construct(&packets[PACKET2], 8, READ_HOLDING_REGISTERS, 0x3211, 8, 8);//sb200 8- 15 (16 back up pump)
  //Data:
  //modbus_construct(&packets[PACKET1], 17, READ_HOLDING_REGISTERS, 0x40000, 7, 0);//ABB 17-21 (16 back up pump)
  //Data:
  //------INVT----------------------
  modbus_construct(&packets[PACKET1], 1, READ_HOLDING_REGISTERS, 0x3000, 8, 0);//invt 1-7
  modbus_construct(&packets[PACKET2], 2, READ_HOLDING_REGISTERS, 0x3000, 8, 8);//invt 1-7
  modbus_construct(&packets[PACKET3], 3, READ_HOLDING_REGISTERS, 0x3000, 8, 16);//invt 1-7
  modbus_construct(&packets[PACKET4], 4, READ_HOLDING_REGISTERS, 0x3000, 8, 24);//invt 1-7
  modbus_construct(&packets[PACKET5], 5, READ_HOLDING_REGISTERS, 0x3000, 8, 32);//invt 1-7
  modbus_construct(&packets[PACKET6], 6, READ_HOLDING_REGISTERS, 0x3000, 8, 40);//invt 1-7
  modbus_construct(&packets[PACKET7], 7, READ_HOLDING_REGISTERS, 0x3000, 8, 48);//invt 1-7
  //  ----------------------
  //------SB-200------------
  modbus_construct(&packets[PACKET8], 8, READ_HOLDING_REGISTERS, 0x3211, 8, 56);
  modbus_construct(&packets[PACKET9], 9, READ_HOLDING_REGISTERS, 0x3211, 8, 64);
  modbus_construct(&packets[PACKET10], 10, READ_HOLDING_REGISTERS, 0x3211, 8, 72);
  modbus_construct(&packets[PACKET11], 11, READ_HOLDING_REGISTERS, 0x3211, 8, 80);
  modbus_construct(&packets[PACKET12], 12, READ_HOLDING_REGISTERS, 0x3211, 8, 88);
  modbus_construct(&packets[PACKET13], 13, READ_HOLDING_REGISTERS, 0x3211, 8, 96);
  modbus_construct(&packets[PACKET14], 14, READ_HOLDING_REGISTERS, 0x3211, 8, 104);
  modbus_construct(&packets[PACKET15], 15, READ_HOLDING_REGISTERS, 0x3211, 8, 112);
  //-----------------------
  
  // Initialize the Modbus Finite State Machine
  modbus_configure(&Serial, baud, SERIAL_8N1, timeout, polling, retry_count, TxEnablePin, packets, TOTAL_NO_OF_PACKETS, regs);

  pinMode(LED, OUTPUT);
  //Modbus Setup done
if(debugging) debug.begin(9600);
  //espsetup
  //wait, send AT
  //if not connected to wifi connect with cipstatus/cwjap?


}

void loop()
{
  modbus_update();
  //check if connected
 
}
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int timeout){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( Serial.available() > 0) Serial.read();    // Clean the input buffer
    Serial.flush();
    if (ATcommand[0] != '\0')
    {
    Serial.println(ATcommand);    // Send the AT command 
    }

    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
    if(Serial.available() != 0){    // if there are data in the UART input buffer, reads it and checks for the asnwer
        response[x] = Serial.read();
        if(debugging){
            debug.print(response[x]);
        }
        x++;
        if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 1;
        }
        else if
        (strstr(response, "ERROR") != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 0;
            break;
        }
    }
    }while((answer == 0) && ((millis() - previous) < timeout));    // Waits for the asnwer with time out

return answer;
}
/*
 //INVT
  for (i = 0; i <=7; i++) {
    j = i * 8;
    debug.print(packets[i].id);
    debug.print(",");
    debug.print(regs[j]);
    debug.print(",");
    debug.print(regs[j+1]);
    debug.print(",");
    debug.print(regs[j+2]);
    debug.print(",");
    debug.print(regs[j+3]);
    debug.print(",");
    debug.print(regs[j+4]);
    debug.print(",");
    debug.print(regs[j+5]);
    debug.print(",");
    debug.print(regs[j+6]);
    debug.print(",");
    debug.println(regs[j+7]);
    //delay(100);
  }
  modbus_update();
  //SB-200
  for (i = 8; i <=15; i++) {
    j = i * 8;
    
    debug.print(packets[i].id);
    debug.print(",");
    debug.print(regs[j]);
    debug.print(",");
    debug.print(regs[j+1]);
    debug.print(",");
    debug.print(regs[j+2]);
    debug.print(",");
    debug.print(regs[j+3]);
    debug.print(",");
    debug.print(regs[j+4]);
    debug.print(",");
    debug.print(regs[j+5]);
    debug.print(",");
    debug.print(regs[j+6]);
    debug.print(",");
    debug.println(regs[j+7]);
   // delay(100);
  }
*/