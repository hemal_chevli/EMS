//Date:20-Aug-15
//send modbus data via esp to webserver
//arduino 1.0.5

//Hardware
//Mega 2560
//serial interfaces
//modbus  Serial1
//debug   Serial0
//4 txEn 
//6 LED status of modbus activity
//esp8266(7,8) Serial2
//reset button
//Power LED
//SD card SPI
//RTC     I2C
//RESET esp8266 switch
  
//ESp flush
//Box open switch
//LED indicators
//modbus disconnected

#include <avr/wdt.h>
#include <SimpleModbusMaster.h>
#include <Wire.h>
#include <Time.h>
#include <DS1307RTC.h>
#include "EmonLib.h"  
#include <SD.h>        
#include <EEPROM.h>

Sd2Card card;
SdVolume volume;
SdFile root;
File Datalog;

EnergyMonitor emon1;                 
EnergyMonitor emon2;                 
EnergyMonitor emon3;                 
EnergyMonitor emon4;                 

#define baud 19200 //modbus baud
//#define timeout 500
#define timeout 10
#define polling 200 // the scan rate
//#define polling 200 // the scan rate
#define retry_count 1000 //tweek this
#define TxEnablePin 21
#define LED 13
// The total amount of available memory on the master to store data
#define TOTAL_NO_OF_REGISTERS 400 //for 100 inverters
#define debugging true
// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS is automatically updated.

enum
{
  PACKET1,
  PACKET2,
  PACKET3,
  PACKET4,
  PACKET5,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];
// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];

//HardwareSerial & debug    = Serial3;
HardwareSerial & debug    = Serial;
HardwareSerial & esp8266  = Serial1;
HardwareSerial & modbus   = Serial2;

//AT commandsstored in flash
const char at[] PROGMEM=            "AT";
const char cipstatus[] PROGMEM =    "AT+CIPSTATUS";
const char rst[] PROGMEM =          "AT+RST";
const char cwjap[] PROGMEM =        "AT+CWJAP=\"HemalChevliOffice\",\"blackcorp\"";
const char cipstart[] PROGMEM =     "AT+CIPSTART=\"TCP\",\"www.pruthvi.co\",80";
const char cipclose[] PROGMEM =     "AT+CIPCLOSE";
const char cifsr[] PROGMEM =        "AT+CIFSR";
const char cipmux[] PROGMEM =       "AT+CIPMUX=0";
//cwmode
//CIPMODE

//pointers to AT commands
const char*  AT PROGMEM = at;
const char*  CIPSTATUS PROGMEM = cipstatus;
const char*  RESET PROGMEM = rst;
const char*  CWJAP PROGMEM = cwjap;
const char*  CIPSTART PROGMEM = cipstart;
const char*  CIPCLOSE PROGMEM = cipclose;
const char*  CIFSR PROGMEM = cifsr;
const char*  CIPMUX PROGMEM = cipmux;
char cmdBuffer[100];

const char POST[] PROGMEM =           "POST /ems/demo-post.php HTTP/1.1";//32 
const char POST_SD[] PROGMEM =        "POST /ems/text-process.php HTTP/1.1";//32 
const char HOST[] PROGMEM =           "Host: pruthvi.co"; //16
const char contentType[] PROGMEM =    "Content-Type: application/json"; //30
const char contentType_SD[] PROGMEM = "Content-Type: application/x-www-form-urlencoded"; //47
const char contentLength[] PROGMEM =  "Content-Length: "; //16 +10(LF of each line) + jsonlenlen(3)  +jsonlen(373)
const int postlen = 105;   //MAKE this automatic
const int postlen_SD = 125;   //MAKE this automatic
const char* const POSTMethod[] PROGMEM = {POST, HOST, contentType, contentLength};
const char* const POSTMethod_SD[] PROGMEM = {POST_SD, HOST, contentType_SD};

int connection;
double Irms1;
double Irms2;
double Irms3;
double Irms4;
//Date Time
tmElements_t tmx;
bool RTCUpdatedFlag= false; //true updated, 0 not updated
int Restarted =1;
int getTimefromNW = 0;

int fileSent = 0; //0 not sent 1 sent
int cipstartStatus;//main
//uint16_t entries; //check if 
//uint16_t sent_entries; //save in eeprom 

unsigned long entries; //check if 
unsigned long sent_entries; //save in eeprom 


long sentTime; //main
bool trasnfer_in_progress = false;
bool SDcardPresent = false;

//Function Declaration
String createJSON(void);
void sendData(int updateRTC);
byte countDigits(int num);
tmElements_t * string_to_tm(tmElements_t *tme, char *str);
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug);
int8_t connStatus(void);
bool netConnected(void);
void save2SD(void);
void sendfromSD(void);
String createDataString(void);
void EEPROMWritelong(int address, long value);
long EEPROMReadlong(long address);

void setup()
{
  if(debugging) debug.begin(115200);
  esp8266.begin(115200);
  //SD setup
  pinMode(53, OUTPUT); //53 is chip select
  if (!SD.begin(53)) {
    debug.println(F("\r\nInitialization failed!"));
    SDcardPresent = false;
    return;
  }
  debug.println(F("\r\nInitialization done."));
  SDcardPresent = true;

  emon1.current(A0, 54);             // Current: input pin, calibration.
  emon2.current(A1, 54);             // Current: input pin, calibration.
  emon3.current(A2, 65);             // Current: input pin, calibration.
  emon4.current(A3, 60);             // Current: input pin, calibration.

  //espsetup
  sendATcommand("AT","OK",1000,debugging);
  delay(2000);//WAIT FOR MODULE TO CONNECT TO AP
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIFSR))); 
  sendATcommand(cmdBuffer,"OK",5000,debugging); //WILL PRINT 0.0.0.0 IF NOT CONNECTED

  //Modbus Setup
  //------INVT----------------------
  modbus_construct(&packets[PACKET1], 1, READ_HOLDING_REGISTERS, 0x3000, 8, 0);
  modbus_construct(&packets[PACKET2], 2, READ_HOLDING_REGISTERS, 0x3000, 8, 8);
  modbus_construct(&packets[PACKET3], 3, READ_HOLDING_REGISTERS, 0x3000, 8, 16);
  modbus_construct(&packets[PACKET4], 4, READ_HOLDING_REGISTERS, 0x3000, 8, 24);
  modbus_construct(&packets[PACKET5], 5, READ_HOLDING_REGISTERS, 0x3000, 8, 32);
  
  // Initialize the Modbus Finite State Machine
  modbus_configure(&Serial2, baud, SERIAL_8E1, timeout, polling, retry_count, TxEnablePin, packets, TOTAL_NO_OF_PACKETS, regs);

  pinMode(LED, OUTPUT);
  //read out contents of SD card
  if(SD.exists("LOG.TXT")){
    fileSent = 0;
    debug.println(F("Read size of file"));
    Datalog = SD.open("LOG.TXT");
    while(Datalog.available()){
      if (Datalog.read() == '\r'){ //start of new dataline
        entries++; //count jsons 
        }
    }
    debug.println(entries);
    entries = 0;
    Datalog.close();
    wdt_enable(WDTO_8S);
  }
  //Read sent entries from eeprom
  debug.println(F("\r\nGoing in Loop----------------------------------------------"));
}
  //STATUS:2 CONNECECTED TO WIFI AND HAS IP
  //STATUS:3 TCP CONNECTION ESTABLISHED IP AND PORT DISPLAYED
  //STATUS:4 DISCONNECTED FROM SERVER
  //STATUS:5 NOT CONNECTED TO AP
  //disconnected cipstart error -> store data on SD card
  //ALREAY CONNECTED
  //busy s...
void loop(){
  wdt_reset();
  //check if day has changed(H = 0) or restart flag 
  if((tmx.Hour == 0 || Restarted) ){
    Restarted = 0;
    getTimefromNW =1;
    debug.println(F("\r\nGetting time from network"));
  }

  connection = connStatus();  
  switch (connection) {
    case 2: case 4:
    wdt_reset();
      //connected to AP and HAS IP
      //DO TCP START
      debug.println(F("CASE:2 or 4 connected to AP and has IP"));
      //close previous connection
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
      sendATcommand(cmdBuffer,"OK",2000,debugging);
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTART)));
      cipstartStatus = sendATcommand(cmdBuffer,"OK",5000,debugging);
      switch (cipstartStatus){
        case 1: //OK
        //net on
        //read SD if content found send all data in file 
        //delete file
        break;
        case 2:  case 3://DNS fail 
          debug.println(F("Internet/Router Down"));
          save2SD();
          sentTime = millis();
          while(millis()-sentTime<10000){
            modbus_update();
          }
        break;
      }//cipstate switch end
      break; //Case2/4 break
    case 3:
    wdt_reset();
     //SEND DATA
      modbus_update();
      if(getTimefromNW == 1){
        sendData(1);
        getTimefromNW = 0;
        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
        sendATcommand(cmdBuffer,"OK",2000,debugging);
      }
      else{
        //send current data
        sendData(0);
        if(fileSent == 0){//file not sent
          sentTime = millis();
          while(millis()-sentTime<10000){ //do this for 10s
            int connection_SD = connStatus();
            switch(connection_SD){
              case 2: case 4:
                wdt_reset();
                debug.println(F("CASE:2 or 4 connected to AP and has IP"));
                //close previous connection
                strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
                sendATcommand(cmdBuffer,"OK",2000,debugging);
                strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTART)));
                sendATcommand(cmdBuffer,"OK",5000,debugging);
              break;
              case 3:
                sendfromSD();
              break;
            }

          }//while loop end
        }
        else{ //if file is sent wait and do modbus update
          sentTime = millis();
          while(millis()-sentTime<10000){
            modbus_update();
          }
        }
      }//case 3 end
      break;
    case 5:
      wdt_reset();
      debug.println(F("CASE 5: Not connected to AP"));
      //NO AP
      //CONNECT TO AP
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CWJAP))); 
      sendATcommand(cmdBuffer,"OK",5000,debugging);
      break;
    default:
    //close connection
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
      sendATcommand(cmdBuffer,"OK",2000,debugging);
      break;
  }//switch end
}//loop end
//////////////////////////////////////////////////////////

///Functions
void sendfromSD(void){
//check if file exits, exit if absent
// check for content
//send line by line 
  
  if(!trasnfer_in_progress){
    if(SD.exists("LOG.TXT")){
      Datalog = SD.open("LOG.TXT");
      Datalog.seek(0);
      while(Datalog.available()){
        if (Datalog.read() == '\r'){ //start of new dataline
          entries++; //count jsons 
        }
      }
      debug.println(entries);
    Datalog.seek(0); //go back to begining of file or close and reopen
    }
    trasnfer_in_progress = true;
    
  }
  //check eeprom for sent entries
  //copy to sententries variable in RAM
  //if present run for loop to jump to appropriate location to send
  if (sent_entries < entries){
    sendData(2);
    sent_entries++;
    debug.println(sent_entries);
    //write sent entries in eeprom 
    EEPROMWritelong(1,sent_entries);
  }
  else if(sent_entries >= entries && fileSent == 0 ){
    debug.println(F("File sent and removed"));
    fileSent = 1;
    trasnfer_in_progress = false;
    entries = 0;
    sent_entries = 0;
    //clear eeprom
    //remove file
    SD.remove("LOG.TXT");
  }  
}

void save2SD(void){
  //get size if more than 100MB dont save
  fileSent = 0;
  Datalog = SD.open("LOG.TXT",FILE_WRITE);//write or append
  if(Datalog){ //if file open
    String machine_data = createDataString();
    Datalog.println(machine_data); //write to file
    Datalog.close(); //close file to save the contents
    //debug.println(machine_data);
    debug.println(F("\r\nData saved to SD"));
    debug.println(machine_data);
  }
  else
    debug.println(F("\r\nError opening file"));
}

byte countDigits(int num){
  byte count=0;
  while(num){
    num=num/10;
    count++;
  }
  return count;
}

void sendData(int command){
  //command = 0 -> Normal send
  //command = 1 -> update RTC
  //command = 2 -> send from SD card
  char date_string[32];
  char *datep = &date_string[0];
  String json;
  int jsonLen;
  int jsonlenlen;
  int cipsendLen;
  //get current form CT
  //Irms1 = emon1.calcIrms(1480);  // Calculate Irms only
  //Irms2 = emon2.calcIrms(1480);  // Calculate Irms only
  //Irms3 = emon3.calcIrms(1480);  // Calculate Irms only
  //Irms4 = emon4.calcIrms(1480);  // Calculate Irms only
  if(command == 0 || command == 1){ //normal send
    json = createJSON();
    debug.println(json);
    jsonLen = json.length();
    jsonlenlen = countDigits(jsonLen);
    cipsendLen = jsonLen + jsonlenlen + postlen ; //when all zeros cipsend is 480
    sprintf(cmdBuffer, "AT+CIPSEND=%d", cipsendLen);
    sendATcommand(cmdBuffer,">",1000,debugging);
    for (int i = 0; i <=2 ; i++){
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[i]))); 
      esp8266.println(cmdBuffer);
      delay(50);
    }
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[3]))); 
    esp8266.print(cmdBuffer);
    esp8266.println(jsonLen);
    esp8266.println();
    delay(50);
    esp8266.print(json);
    sendATcommand(" ","SEND OK",5000,debugging); //send compelete
  }
  else if(command == 2){ //sending from SD card
    json = Datalog.readStringUntil('\n');
    debug.println(json);
    jsonLen = json.length()+6; //6 for var_a=
    jsonlenlen = countDigits(jsonLen);
    cipsendLen = jsonLen + jsonlenlen + postlen_SD ;   
    sprintf(cmdBuffer, "AT+CIPSEND=%d", cipsendLen);
    sendATcommand(cmdBuffer,">",1000,debugging);
    for (int i = 0; i <=2 ; i++){
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod_SD[i]))); 
      esp8266.println(cmdBuffer);
      delay(50);
    }
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[3]))); 
    esp8266.print(cmdBuffer);
    esp8266.println(jsonLen);
    esp8266.println();
    delay(50);
    esp8266.print("var_a=");
    esp8266.print(json);
    sendATcommand(" ","SEND OK",5000,debugging); //send compelete
  }
  //Parse the header
  if(command == 1 && !(RTCUpdatedFlag)){
    //update only if data is available from the network
     //Parse the returned header & web page. Looking for 'Date' line in header
    if (esp8266.find("Date: ")) //get the date line from the http header (for example)
    {
      int i;
      for (i = 0; i < 31; i++) //31 this should capture the 'Date: ' line from the header
      {
        if (esp8266.available())  //new characters received?
        {
          char c = esp8266.read(); //print to console
          debug.write(c);
          date_string[i] = c;
        }
        else i--;  //if not, keep going round loop until we've got all the characters
      }
    }
    string_to_tm(&tmx, date_string);
    //update RTC
    if(RTC.write(tmx)){
      RTCUpdatedFlag = true;
      debug.println(F("RTC Updated"));
    }
    if (RTC.read(tmx)) {
      debug.print(F("Ok, Time = "));
      debug.print(tmx.Hour);
      debug.write(':');
      debug.print(tmx.Minute);
      debug.write(':');
      debug.print(tmx.Second);
      debug.print(F(", Date (D/M/Y) = "));
      debug.print(tmx.Day);
      debug.write('/');
      debug.print(tmx.Month);
      debug.write('/');
      debug.print(tmYearToCalendar(tmx.Year));
      debug.println();
    } 
  }//RTC update end

  //close connection
  //strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
  //sendATcommand(cmdBuffer,"OK",2000,debugging);
  
  /*while(!(netConnected())){
    delay(10000);
  }*/
  
}
int8_t connStatus(void){
    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTATUS))); 
    esp8266.println(cmdBuffer); //send cipstatus command
    x = 0;
    previous = millis();
    // this loop waits for the answer
    do{
        if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
            response[x] = esp8266.read();
            if(debugging){
                debug.print(response[x]);
            }
            x++;
            if      (strstr(response, "STATUS:2") != NULL)    
            {
                answer = 2;
            }
            else if (strstr(response, "STATUS:3") != NULL)  
            {
                answer = 3;
            }
            else if (strstr(response, "STATUS:4") != NULL)  
            {
                answer = 4;
            }
            else if (strstr(response, "STATUS:5") != NULL)  
            {
                answer = 5;
            }
        }
    }while((answer == 0) && ((millis() - previous) < 5000));    // Waits for the asnwer with time out

    return answer;
}
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    if (ATcommand[0] != '\0')
    {
    esp8266.println(ATcommand);    // Send the AT command 
    }

    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
    if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
        response[x] = esp8266.read();
        if(isDebug && debugging){
            debug.print(response[x]);
        }
        x++;
        if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 1;
        }
        else if(strstr(response, "DNS Fail") != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 2;
            break;
        }
        else if(strstr(response, "no ip") != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 3;
            break;
        }
        else if(strstr(response, "ERROR") != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 0;
            break;
        }
    }
    }while((answer == 0) && ((millis() - previous) < Timeout));    // Waits for the asnwer with time out
  return answer;
}

String createJSON(void){
  //Add API key
  //Add SD present flag
  //print two digits
  String json;
  int i,j;

  RTC.read(tmx);
  json = "{";
  json += "\"DT\":[\"";
  json += tmYearToCalendar(tmx.Year);
  json += "-";
  if(tmx.Month >0 && tmx.Month <10){
    json += "0";
  }
  json += tmx.Month;
  json += "-";
  if(tmx.Day >0 && tmx.Day <10){
    json += "0";
  }
  json += tmx.Day;
  json += "\",\"";
  if(tmx.Hour >=0 && tmx.Hour <10){
    json += "0";
  }
  json += tmx.Hour;
  json += ":";
  if(tmx.Minute >=0 && tmx.Minute <10){
    json += "0";
  }
  json += tmx.Minute;
  json += ":";
  if(tmx.Second >=0 && tmx.Second <10){
    json += "0";
  }
  json += tmx.Second;
  json +="\"],";
  for(i= 0 ;i<5;i++){
    j = i * 8;
    json+="\"";
    json+= packets[i].id;
    json+="\":";
    json+="[";
    json+=packets[i].id;
    json+=",";
    json+= regs[j];
    json+=",";
    json+= regs[j+1];
    json+=",";
    json+= regs[j+2];
    json+=",";
    json+= regs[j+3];
    json+=",";
    json+= regs[j+4];
    json+=",";
    json+= regs[j+5];
    json+=",";
    json+= regs[j+6];
    json+=",";
    json+= regs[j+7]; 
    json+="]";
      if(i<4){
        json+=",";
      }
    }
  json+="}";

  return json;
}

String createDataString(void){
  String machine_data;
  int i,j;
  //Add line number
  RTC.read(tmx);
  //2015-12-10,15:34:23,1,d1,d2,d3,d4,d5,d6,d7,d8,2,d1,d2,d3\n
  //2015-12-10,15:34:23,1,d1,d2,d3,d4,d5,d6,d7,d8,2,d1,d2,d3\n
  //2015-12-10,15:34:23,1,d1,d2,d3,d4,d5,d6,d7,d8,2,d1,d2,d3\n
  machine_data = String(tmYearToCalendar(tmx.Year));
  machine_data += "-";
  if(tmx.Month >0 && tmx.Month <10){
    machine_data += "0";
  }
  machine_data += tmx.Month;
  machine_data += "-";
  if(tmx.Day >0 && tmx.Day <10){
    machine_data += "0";
  }
  machine_data += tmx.Day;
  machine_data += ",";
  if(tmx.Hour >=0 && tmx.Hour <10){
    machine_data += "0";
  }
  machine_data += tmx.Hour;
  machine_data += ":";
  if(tmx.Minute >=0 && tmx.Minute <10){
    machine_data += "0";
  }
  machine_data += tmx.Minute;
  machine_data += ":";
  if(tmx.Second >=0 && tmx.Second <10){
    machine_data += "0";
  }
  machine_data += tmx.Second;
  machine_data +=",";
  for(i= 0 ;i<5;i++){
    j = i * 8;
    machine_data+=packets[i].id;
    machine_data+=",";
    machine_data+= regs[j];
    machine_data+=",";
    machine_data+= regs[j+1];
    machine_data+=",";
    machine_data+= regs[j+2];
    machine_data+=",";
    machine_data+= regs[j+3];
    machine_data+=",";
    machine_data+= regs[j+4];
    machine_data+=",";
    machine_data+= regs[j+5];
    machine_data+=",";
    machine_data+= regs[j+6];
    machine_data+=",";
    machine_data+= regs[j+7]; 
    if(i<4){
      machine_data+=",";
    }
  }
  return machine_data;
}

tmElements_t * string_to_tm(tmElements_t *tme, char *str) {
  // Sat, 28 Mar 2015 13:53:38 GMT

  const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

  char *r, *i, *t;
  r = strtok_r(str, " ", &i);

  r = strtok_r(NULL, " ", &i);
  tme->Day = atoi(r);

  r = strtok_r(NULL, " ", &i);
  for (int i = 0; i < 12; i++) {
    if (!strcmp(months[i], r)) {
      tme->Month = i + 1;
      break;
    }
  }

  r = strtok_r(NULL, " ", &i);
  tme->Year = atoi(r) - 1970;

  r = strtok_r(NULL, " ", &i);
  t = strtok_r(r, ":", &i);
  tme->Hour = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Minute = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Second = atoi(t);

  return tme;
}
//This function will write a 4 byte (32bit) long to the eeprom at
//the specified address to address + 3.
void EEPROMWritelong(int address, long value){
  //Decomposition from a long to 4 bytes by using bitshift.
  //One = Most significant -> Four = Least significant byte
  byte four = (value & 0xFF);
  byte three = ((value >> 8) & 0xFF);
  byte two = ((value >> 16) & 0xFF);
  byte one = ((value >> 24) & 0xFF);

  //Write the 4 bytes into the eeprom memory.
  EEPROM.write(address, four);
  EEPROM.write(address + 1, three);
  EEPROM.write(address + 2, two);
  EEPROM.write(address + 3, one);
}

long EEPROMReadlong(long address){
  //Read the 4 bytes from the eeprom memory.
  long four = EEPROM.read(address);
  long three = EEPROM.read(address + 1);
  long two = EEPROM.read(address + 2);
  long one = EEPROM.read(address + 3);

  //Return the recomposed long by using bitshift.
  return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
}

/*
WORKING POST LOOP SNIPPET
//do switch on cipstatus
  if(sendATcommand("AT+CIPSTATUS","STATUS:4",1000,debugging)){
      sendATcommand("AT+CIPSTART=\"TCP\",\"50.87.144.195\",80","CONNECT",1000,debugging);
  }
  if(sendATcommand("AT+CIPSTATUS","STATUS:3",1000,debugging)){//CONNECTED TO WEBSERVER
    if(sendATcommand("AT+CIPSEND=152",">",1000,debugging)){
      esp8266.println("POST /sandbox/posttest.php HTTP/1.1");
      esp8266.println("Host: black-electronics.com");
      esp8266.println("Content-Type: application/x-www-form-urlencoded");
      esp8266.println("Content-Length: 15");
      esp8266.println("");
      esp8266.println("var_a=3&var_b=5"); 
      //read reply
      sendATcommand("","200 OK",10000,debugging);
      }
    }
    delay(3000);
---------------------
*/
/*
bool netConnected(void){
  sprintf(cmdBuffer,"AT+CIPSTART=\"TCP\",\"www.black-electronics.com\",80");
  sendATcommand(cmdBuffer,"OK",20000,debugging);
  sprintf(cmdBuffer,"AT+CIPSEND=53");
  sendATcommand(cmdBuffer,">",2000,debugging);
  sprintf(cmdBuffer,"GET http://black-electronics.com/sandbox/check1.php");
  if(sendATcommand(cmdBuffer,"INTERNET OK",2000,debugging)){
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
    sendATcommand(cmdBuffer,"OK",2000,debugging);
    return true;

  }
  else{
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
    sendATcommand(cmdBuffer,"OK",2000,debugging);
    return false;
  }
}*/