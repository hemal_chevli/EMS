//Date:20-Aug-15
//send modbus data via esp to webserver
//arduino 1.0.5

//Hardware
//Mega 2560
//serial interfaces
//modbus  Serial1
//debug   Serial0
//4 txEn 
//6 LED status of modbus activity
//esp8266(7,8) Serial2
//reset button
//Power LED
//SD card SPI
//RTC     I2C

//send static data first and connected modbus
//wire rct
//wire sd card

#include <SimpleModbusMaster.h>
#include <Wire.h>
#include "EmonLib.h"                 
EnergyMonitor emon1;                 

//#include <SD.h>
#define baud 19200 //modbus baud
//#define timeout 500
#define timeout 1000
#define polling 200 // the scan rate
//#define polling 200 // the scan rate
#define retry_count 1000 //tweek this
// used to toggle the receive/transmit pin on the driver
//#define TxEnablePin 2 //MEGA SHIELD
#define TxEnablePin 15
#define LED 13
// The total amount of available memory on the master to store data
#define TOTAL_NO_OF_REGISTERS 150
#define debugging true
// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS is automatically updated.
enum
{
  PACKET1,
  PACKET2,
  PACKET3,
  PACKET4,
  PACKET5,
  PACKET6,
  PACKET7,
  PACKET8,
  PACKET9,
  PACKET10,
  PACKET11,
  PACKET12,
  PACKET13,
  PACKET14,
  PACKET15,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];
// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];
/*MEGA SHIELD
DEBUG SERIAL
ESP SERIAL1
MODBUS SERIAL 2
*/
HardwareSerial & debug    = Serial;
HardwareSerial & esp8266  = Serial1;
HardwareSerial & modbus   = Serial2;

//AT commandsstored in flash
const char at[] PROGMEM=            "AT";
const char cipstatus[] PROGMEM =    "AT+CIPSTATUS";
const char rst[] PROGMEM =          "AT+RST";
const char cwjap[] PROGMEM =        "AT+CWJAP=\"EMS\",\"9824130336\"";
//const char cwjap[] PROGMEM =        "AT+CWJAP=\"EMS\",\"9824130336\"";
const char cipstart[] PROGMEM =     "AT+CIPSTART=\"TCP\",\"128.199.107.106\",80";
const char cipclose[] PROGMEM =     "AT+CIPCLOSE";
const char cifsr[] PROGMEM =        "AT+CIFSR";
const char cipmux[] PROGMEM =       "AT+CIPMUX=0";
//cwmode
//CIPMODE

//pointers to AT commands
const char*  AT PROGMEM = at;
const char*  CIPSTATUS PROGMEM = cipstatus;
const char*  RESET PROGMEM = rst;
const char*  CWJAP PROGMEM = cwjap;
const char*  CIPSTART PROGMEM = cipstart;
const char*  CIPCLOSE PROGMEM = cipclose;
const char*  CIFSR PROGMEM = cifsr;
const char*  CIPMUX PROGMEM = cipmux;
char cmdBuffer[100];

const char POST[] PROGMEM =           "POST /ems/demo_post3.php HTTP/1.1";//32 
const char HOST[] PROGMEM =           "Host: pruthvi.co"; //16
const char contentType[] PROGMEM =    "Content-Type: application/json"; //30
const char contentLength[] PROGMEM =  "Content-Length: "; //16 +10(LF) + jsonlenlen(3)  +jsonlen(373)
const int postlen= 106; //104
const char* const POSTMethod[] PROGMEM = {POST, HOST, contentType, contentLength};

int cipsendLen=0;
int connection;
String json;
int i,j;
double Irms1;



//Function Declaration
void createJSON(void);
void sendData(void);
byte countDigits(int num);

void setup()
{
emon1.current(A0, 54);             // Current: input pin, calibration.

  if(debugging) debug.begin(115200);
  esp8266.begin(115200);
  //espsetup
  sendATcommand("AT","OK",1000,debugging);
  
  delay(2000);//WAIT FOR MODULE TO CONNECT TO AP
  
  //get ip
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIFSR))); 
  sendATcommand(cmdBuffer,"OK",5000,debugging); //WILL PRINT 0.0.0.0 IF NOT CONNECTED

  modbus_construct(&packets[PACKET1], 2, READ_HOLDING_REGISTERS, 0x3000, 8, 0);//invt 1-7
  modbus_construct(&packets[PACKET2], 3, READ_HOLDING_REGISTERS, 0x3000, 8, 8);//invt 1-7
  modbus_construct(&packets[PACKET3], 4, READ_HOLDING_REGISTERS, 0x3000, 8, 16);//invt 1-7
  modbus_construct(&packets[PACKET4], 5, READ_HOLDING_REGISTERS, 0x3000, 8, 24);//invt 1-7
  modbus_construct(&packets[PACKET5], 6, READ_HOLDING_REGISTERS, 0x3000, 8, 32);//invt 1-7
  modbus_construct(&packets[PACKET6], 7, READ_HOLDING_REGISTERS, 0x3000, 8, 40);//invt 1-7
  modbus_construct(&packets[PACKET7], 8, READ_HOLDING_REGISTERS, 0x3000, 8, 48);//invt 1-7
  modbus_construct(&packets[PACKET8], 9, READ_HOLDING_REGISTERS, 0x3000, 8, 56);//invt 1-7
  modbus_construct(&packets[PACKET9], 10, READ_HOLDING_REGISTERS, 0x3000, 8, 64);//invt 1-7
  modbus_construct(&packets[PACKET10], 11, READ_HOLDING_REGISTERS, 0x3000, 8, 72);//invt 1-7
  modbus_construct(&packets[PACKET11], 15, READ_HOLDING_REGISTERS, 0x3211, 8, 80);//SB-200
  modbus_construct(&packets[PACKET12], 16, READ_HOLDING_REGISTERS, 0x2102, 7, 88);//Delta
  modbus_construct(&packets[PACKET13], 17, READ_HOLDING_REGISTERS, 0x3000, 8, 96);//invt
  modbus_construct(&packets[PACKET14], 18, READ_HOLDING_REGISTERS, 0x2102, 7, 104);//delta B
  modbus_construct(&packets[PACKET15], 19, READ_HOLDING_REGISTERS, 0x2102, 7, 112);//delta-M

  // Initialize the Modbus Finite State Machine
  modbus_configure(&Serial2, baud, SERIAL_8E1, timeout, polling, retry_count, TxEnablePin, packets, TOTAL_NO_OF_PACKETS, regs);

  pinMode(LED, OUTPUT);
  debug.println(F("\r\nGoing in Loop----------------------------------------------"));
}

void loop(){
  modbus_update();
  connection = connStatus();
  //STATUS:2 CONNECECTED TO WIFI AND HAS IP
  //STATUS:3 TCP CONNECTION ESTABLISHED IP AND PORT DISPLAYED
  //STATUS:4 DISCONNECTED FROM SERVER
  //STATUS:5 NOT CONNECTED TO AP
  //ALREAY CONNECTED
  //busy s...
  
  switch (connection) {
      case 2:
        //connected to AP and HAS IP
        //DO TCP START
        debug.println("case 2");
        //close previous connection
        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
        sendATcommand(cmdBuffer,"OK",2000,debugging);

        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTART))); 
        sendATcommand(cmdBuffer,"OK",20000,debugging);//CONNECT OK

        break;
      case 3:
        //SEND DATA
        modbus_update();
        sendData();
        break;
      case 4:
        // DICONNECTED
        // DO TCP START
        debug.println("case 4");
        //close
        //strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
        //sendATcommand(cmdBuffer,"OK",2000,debugging);
        //open
        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTART))); 
        sendATcommand(cmdBuffer,"OK",5000,debugging);
        break;
      case 5:
        debug.println("case 5");
        //NO AP
        //CONNECT TO AP
        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CWJAP))); 
        sendATcommand(cmdBuffer,"OK",5000,debugging);
        
        break;
      default:
      //close connection
        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
        sendATcommand(cmdBuffer,"OK",2000,debugging);
        break;
  }//switch end
}//loop end
byte countDigits(int num){
  byte count=0;
  while(num){
    num=num/10;
    count++;
  }
  return count;
} 
void sendData(void){
  
  int answer=0;
  //get current form CT
  Irms1 = emon1.calcIrms(1480);  // Calculate Irms only

  createJSON();
  debug.println(json);
  int jsonLen = json.length();
  int jsonlenlen = countDigits(jsonLen);
  cipsendLen = jsonLen + jsonlenlen + postlen ; //when all zeros cipsend is 480
  sprintf(cmdBuffer, "AT+CIPSEND=%d", cipsendLen);
    //Serial.println(buf);
  sendATcommand(cmdBuffer,">",1000,debugging);
  for (int i = 0; i <=2 ; i++){
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[i]))); 
    esp8266.println(cmdBuffer);
    delay(50);
  }
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[3]))); 
  esp8266.print(cmdBuffer);
  esp8266.println(jsonLen);
  esp8266.println();
  delay(50);
  esp8266.print(json);
  delay(2000); //wait for reply
  //read SEND OK, if error resend
  sendATcommand(" ","SEND OK",5000,debugging);
  //while 10 sec call modbus and read if possible
 long sentTime= millis();
  while(millis()-sentTime<10000){
    modbus_update();
    //read reply here
  }
  //close connection
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
  sendATcommand(cmdBuffer,"OK",2000,debugging);
  //delay(5000);
}
int8_t connStatus(){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTATUS))); 
    esp8266.println(cmdBuffer); //send cipstatus command
    
    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
        if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
            response[x] = esp8266.read();
            if(debugging){
                debug.print(response[x]);
            }
            x++;
            if      (strstr(response, "STATUS:2") != NULL)    
            {
                answer = 2;
            }
            else if (strstr(response, "STATUS:3") != NULL)  
            {
                answer = 3;
            }
            else if (strstr(response, "STATUS:4") != NULL)  
            {
                answer = 4;
            }
            else if (strstr(response, "STATUS:5") != NULL)  
            {
                answer = 5;
            }
            
        }
    }while((answer == 0) && ((millis() - previous) < 5000));    // Waits for the asnwer with time out

    return answer;
}
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    if (ATcommand[0] != '\0')
    {
    esp8266.println(ATcommand);    // Send the AT command 
    }

    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
    if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
        response[x] = esp8266.read();
        if(isDebug && debugging){
            debug.print(response[x]);
        }
        x++;
        if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 1;
        }
        else if
        (strstr(response, "ERROR") != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 0;
            break;
        }
    }
    }while((answer == 0) && ((millis() - previous) < Timeout));    // Waits for the asnwer with time out

return answer;
}


void createJSON(void){
  json="{";
  for(i= 0 ;i<15;i++){
    j = i * 8;
    json+="\"";
    json+= packets[i].id;
    json+="\":";
    json+="[";
    json+=packets[i].id;
    json+=",";
    json+= regs[j];
    json+=",";
    json+= regs[j+1];
    json+=",";
    json+= regs[j+2];
    json+=",";
    json+= regs[j+3];
    json+=",";
    json+= regs[j+4];
    json+=",";
    json+= regs[j+5];
    json+=",";
    json+= regs[j+6];
    json+=",";
    json+= regs[j+7];
    json+="],";
    }
  //,"I1":9.33,"I2":12.31,"I4":12.31,"I4":12.31}
  json+="\"50\":[50,";
  json+=Irms1;
  json+="]}";

}
