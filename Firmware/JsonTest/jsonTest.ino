#include <SimpleModbusMaster.h>
//create json post request
//http://pruthvi.co/ems/demo_post.php
//http://pruthvi.co/ems/data_view.php
//////////////////// Port information ///////////////////
#define baud 19200 //modbus baud
#define timeout 500
//#define timeout 1000
#define polling 1000 // the scan rate
//#define polling 200 // the scan rate
#define retry_count 10

// used to toggle the receive/transmit pin on the driver
#define TxEnablePin 21

#define LED 13

// The total amount of available memory on the master to store data
#define TOTAL_NO_OF_REGISTERS 170



// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS is automatically updated.
enum
{
  PACKET1,
  PACKET2,
  PACKET3,
  PACKET4,
  PACKET5,
  PACKET6,
  PACKET7,
  PACKET8,
  PACKET9,
  PACKET10,
  PACKET11,
  PACKET12,
  PACKET13,
  PACKET14,
  PACKET15,
  PACKET16,
  PACKET17,
  PACKET18,
  PACKET19,
  PACKET20,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];
long lastPeriod = 0;
HardwareSerial & debug    = Serial;

const long interval = 5000;           // interval at which to blink (milliseconds)
unsigned long previousMillis = 0;        // will store last time LED was updated
int i, j;
String json;
//128.199.107.106,80
//POST /ems/demo_post.php HTTP/1.1\r\n
//Host: pruthvi.co\r\n
//Content-Type: application/json
//Content-Length: 373

const char POST[] PROGMEM =           "POST /ems/demo_post.php HTTP/1.1";//32 
const char HOST[] PROGMEM =           "Host: pruthvi.co"; //16
const char contentType[] PROGMEM =    "Content-Type: application/json"; //30
const char contentLength[] PROGMEM =  "Content-Length: "; //16 +10(LF) + jsonlenlen(3)  +jsonlen(373)
const int postlen= 104;
const char* const POSTMethod[] PROGMEM = {POST, HOST, contentType, contentLength};

void createJson(void);
void sendData(void);
byte countDigits(int num);
void setup() {
  Serial.begin(115200);
  Serial.println("Start");
  sendData();
}

void loop() {
  // not used in this example
}

byte countDigits(int num){
  byte count=0;
  while(num){
    num=num/10;
    count++;
  }
  return count;
}

void sendData(void){
   createJson();
   int jsonLen = json.length();
   int jsonlenlen = countDigits(jsonLen);
   cipsendLen = jsonLen+jsonlenlen+ postlen ;
   String cipsend = "AT+CIPSEND=";
   cipsend+=cipsendLen;

  Serial.println(cipsend);

   for (int i = 0; i <=2 ; i++){
        strcpy_P(DataBuffer, (char*)pgm_read_word(&(POSTMethod[i]))); 
        Serial.println(DataBuffer);
        delay(50);
    }
    strcpy_P(DataBuffer, (char*)pgm_read_word(&(POSTMethod[3]))); 
    Serial.print(DataBuffer);
    Serial.println(jsonLen);
    Serial.println(" ");
    Serial.print(json);
}
void createJSON(void){
  json="{";
  for(i= 0 ;i<4;i++){
    j = i * 6;
    json+="\"";
    json+= packets[i].id;
    json+="\":";
    json+="[";
    json+=packets[i].id;
    json+=",";
    json+= regs[j];
    json+=",";
    json+= regs[j+1];
    json+=",";
    json+= regs[j+2];
    json+=",";
    json+= regs[j+3];
    json+=",";
    json+= regs[j+4];
    json+=",";
    json+= regs[j+5];
    json+="],";
    }
  for(i= 4 ;i<17;i++){
    j = i * 8;
    json+="\"";
    json+= packets[i].id;
    json+="\":";
    json+="[";
    json+=packets[i].id;
    json+=",";
    json+= regs[j];
    json+=",";
    json+= regs[j+1];
    json+=",";
    json+= regs[j+2];
    json+=",";
    json+= regs[j+3];
    json+=",";
    json+= regs[j+4];
    json+=",";
    json+= regs[j+5];
    json+=",";
    json+= regs[j+6];
    json+=",";
    json+= regs[j+7];
    json+="],";
    }
    for(i= 0 ;i<4;i++){
    j = i * 8;
    json+="\"";
    json+= packets[i].id;
    json+="\":";
    json+="[";
    json+=packets[i].id;
    json+=",";
    json+= regs[j];
    json+=",";
    json+= regs[j+1];
    json+=",";
    json+= regs[j+2];
    json+=",";
    json+= regs[j+3];
    json+=",";
    json+= regs[j+4];
    json+=",";
    json+= regs[j+5];
    json+=",";
    json+= regs[j+6];
    json+="],";
    }
  //,"I1":9.33,"I2":12.31,"I4":12.31,"I4":12.31}
  json+=",\"I1\":";
  json+=Irms1;
  json+=",\"I2\":";
  json+=Irms1;
  json+=",\"I3\":";
  json+=Irms1;
  json+=",\"I4\":";
  json+=Irms1;
  json+="}";
}//
//{"1":[1,2,3,4,5,6,7,8,9],"2":[2,2,3,4,5,6,7,8,9],"3":[3,2,3,4,5,6,7,8,9],"4":[4,2,3,4,5,6,7,8,9],"5":[5,2,3,4,5,6,7,8,9],"6":[6,2,3,4,5,6,7,8,9],"7":[7,2,3,4,5,6,7,8,9],"8":[8,2,3,4,5,6,7,8,9],"9":[9,2,3,4,5,6,7,8,9],"10":[10,2,3,4,5,6,7,8,9],"11":[11,2,3,4,5,6,7,8,9],"12":[12,2,3,4,5,6,7,8,9],"13":[13,2,3,4,5,6,7,8,9],"14":[14,2,3,4,5,6,7,8,9],"15":[15,2,3,4,5,6,7,8,9]}