#include <SimpleModbusMaster.h>
#include <SoftwareSerial.h>
/*
   The example will use PACKET1 to read a register from address 0 (the adc ch0 value)
   from the arduino slave (id=1). It will then use this value to adjust the brightness
   of an led on pin 9 using PWM.
   It will then use packet2 to write a register (its own adc ch0 value) to address 1
   on the arduino slave (id=1) adjusting the brightness of an led on pin 9 using PWM.

 Recommended Modbus slave:
 *  diagslave http://www.modbusdriver.com/diagslave.html
 *
 *  In a Linux box, run
 *  "./diagslave /dev/ttyUSB0 -b 19200 -d 8 -s 2 -p none -m rtu -a 1"
 *  This is:
 *    serial port /dev/ttyUSB0 at 19200 baud 8N2
 *    RTU mode and address @1


 */

//////////////////// Port information ///////////////////
#define baud 19200 //modbus baud
#define timeout 500
//#define timeout 1000
#define polling 200 // the scan rate
//#define polling 200 // the scan rate
#define retry_count 1000

// used to toggle the receive/transmit pin on the driver
#define TxEnablePin 21

#define LED 13

// The total amount of available memory on the master to store data
#define TOTAL_NO_OF_REGISTERS 350



// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS is automatically updated.
enum
{
  PACKET1,
  PACKET2,
  PACKET3,
  PACKET4,
  PACKET5,
  PACKET6,
  PACKET7,
  PACKET8,
  PACKET9,
  PACKET10,
  PACKET11,
  PACKET12,
  PACKET13,
  PACKET14,
  PACKET15,
  PACKET16,
  PACKET17,
  PACKET18,
  PACKET19,
  PACKET20,
  PACKET21,
  PACKET22,
  PACKET23,
  PACKET24,
  PACKET25,
  PACKET26,
  PACKET27,
  PACKET28,
  PACKET29,
  PACKET30,
  PACKET31,
  PACKET32,
  PACKET33,
  PACKET34,
  PACKET35,
  PACKET36,
  PACKET37,
  PACKET38,
  PACKET39,
  PACKET40,
  PACKET41,
  PACKET42,
  PACKET43,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];
long lastPeriod = 0;
HardwareSerial & debug    = Serial;
int i, j;
const long interval = 5000;           // interval at which to blink (milliseconds)
unsigned long previousMillis = 0;        // will store last time LED was updated
void setup()
{
  //modbus_construct(packet,slaveid,command,reg address,datalen,local reg storage index)
  // modbus_construct(&packets[PACKET1], 2, READ_HOLDING_REGISTERS, 0x3000, 8, 0);//invt 1-7
  //Data:op freq,ref freq,dc bus voltage,op voltage,op current,rotation speed,power,torque
  // modbus_construct(&packets[PACKET2], 8, READ_HOLDING_REGISTERS, 0x3211, 8, 8);//sb200 8- 15 (16 back up pump)
  //Data:
  //modbus_construct(&packets[PACKET1], 17, READ_HOLDING_REGISTERS, 0x40000, 7, 0);//ABB 17-21 (16 back up pump)
  //Data:

  //------YASKAWA----------------------
  modbus_construct(&packets[PACKET1], 27, READ_HOLDING_REGISTERS, 0x0023, 6, 0);
  modbus_construct(&packets[PACKET2], 28, READ_HOLDING_REGISTERS, 0x0023, 6, 6);
  modbus_construct(&packets[PACKET3], 29, READ_HOLDING_REGISTERS, 0x0023, 6, 12);
  modbus_construct(&packets[PACKET4], 30, READ_HOLDING_REGISTERS, 0x0023, 6, 18);
  //------SB-200------------
  modbus_construct(&packets[PACKET5],  31, READ_HOLDING_REGISTERS, 0x3211, 8, 32);
  modbus_construct(&packets[PACKET6],  32, READ_HOLDING_REGISTERS, 0x3211, 8, 40);
  modbus_construct(&packets[PACKET7],  33, READ_HOLDING_REGISTERS, 0x3211, 8, 48);
  modbus_construct(&packets[PACKET8],  34, READ_HOLDING_REGISTERS, 0x3211, 8, 56);
  modbus_construct(&packets[PACKET9],  35, READ_HOLDING_REGISTERS, 0x3211, 8, 64);
  modbus_construct(&packets[PACKET10], 36, READ_HOLDING_REGISTERS, 0x3211, 8, 72);
  modbus_construct(&packets[PACKET11], 37, READ_HOLDING_REGISTERS, 0x3211, 8, 80);
  modbus_construct(&packets[PACKET12], 38, READ_HOLDING_REGISTERS, 0x3211, 8, 88);
  //------INVT----------------------
  modbus_construct(&packets[PACKET13], 39, READ_HOLDING_REGISTERS, 0x3000, 8, 96);
  modbus_construct(&packets[PACKET14], 40, READ_HOLDING_REGISTERS, 0x3000, 8, 104);
  modbus_construct(&packets[PACKET15], 41, READ_HOLDING_REGISTERS, 0x3000, 8, 112);
  modbus_construct(&packets[PACKET16], 42, READ_HOLDING_REGISTERS, 0x3000, 8, 120);
  modbus_construct(&packets[PACKET17], 43, READ_HOLDING_REGISTERS, 0x3000, 8, 128);
  //------DELTA----------------------
  modbus_construct(&packets[PACKET18], 44, READ_HOLDING_REGISTERS, 0x2102, 8, 136);
  modbus_construct(&packets[PACKET19], 45, READ_HOLDING_REGISTERS, 0x2102, 7, 144);
  modbus_construct(&packets[PACKET20], 46, READ_HOLDING_REGISTERS, 0x2102, 7, 152);
  //------Stenter 2--------------------
  modbus_construct(&packets[PACKET21], 21, READ_HOLDING_REGISTERS, 0x2102, 7, 160);//DELTA-B
  modbus_construct(&packets[PACKET22], 15, READ_HOLDING_REGISTERS, 0x3000, 8, 168);//INVT
  modbus_construct(&packets[PACKET23], 16, READ_HOLDING_REGISTERS, 0x3000, 8, 176);//INVT
  modbus_construct(&packets[PACKET24], 17, READ_HOLDING_REGISTERS, 0x3000, 8, 184);//INVT
  modbus_construct(&packets[PACKET25], 18, READ_HOLDING_REGISTERS, 0x3000, 8, 192);//INVT
  modbus_construct(&packets[PACKET26], 19, READ_HOLDING_REGISTERS, 0x3000, 8, 200);//INVT
  modbus_construct(&packets[PACKET27], 20, READ_HOLDING_REGISTERS, 0x3000, 8, 208);//INVT
  modbus_construct(&packets[PACKET28], 22, READ_HOLDING_REGISTERS, 0x2102, 8, 216);//INVT
  modbus_construct(&packets[PACKET29], 23, READ_HOLDING_REGISTERS, 0x3000, 8, 224);//INVT
  modbus_construct(&packets[PACKET30], 24, READ_HOLDING_REGISTERS, 0x3000, 8, 232);//INVT
  modbus_construct(&packets[PACKET31], 25, READ_HOLDING_REGISTERS, 0x3000, 8, 240);//INVT
  //------Stenter 1--------------------
  modbus_construct(&packets[PACKET32], 2, READ_HOLDING_REGISTERS, 0x3000, 8, 248);//INVT
  modbus_construct(&packets[PACKET33], 3, READ_HOLDING_REGISTERS, 0x3000, 8, 256);//INVT
  modbus_construct(&packets[PACKET34], 4, READ_HOLDING_REGISTERS, 0x3000, 8, 264);//INVT
  modbus_construct(&packets[PACKET35], 5, READ_HOLDING_REGISTERS, 0x3000, 8, 272);//INVT
  modbus_construct(&packets[PACKET36], 6, READ_HOLDING_REGISTERS, 0x3000, 8, 280);//INVT
  modbus_construct(&packets[PACKET37], 7, READ_HOLDING_REGISTERS, 0x3000, 8, 288);//INVT
  modbus_construct(&packets[PACKET38], 8, READ_HOLDING_REGISTERS, 0x2102, 7, 296);//Delta
  modbus_construct(&packets[PACKET39], 9, READ_HOLDING_REGISTERS, 0x3211, 8, 304);//SB-200
  modbus_construct(&packets[PACKET40], 10, READ_HOLDING_REGISTERS, 0x3211, 8, 312);//SB-200
  modbus_construct(&packets[PACKET41], 11, READ_HOLDING_REGISTERS, 0x3211, 8, 320);//SB-200
  modbus_construct(&packets[PACKET42], 12, READ_HOLDING_REGISTERS, 0x3211, 8, 328);//SB-200
  modbus_construct(&packets[PACKET43], 13, READ_HOLDING_REGISTERS, 0x3211, 8, 336);//SB-200
  
  // Initialize the Modbus Finite State Machine
  modbus_configure(&Serial3, baud, SERIAL_8E1, timeout, polling, retry_count, TxEnablePin, packets, TOTAL_NO_OF_PACKETS, regs);

  pinMode(LED, OUTPUT);
  debug.begin(115200);

}

void loop()
{
  modbus_update();
   unsigned long currentMillis = millis();
  if(currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;   
    //Yaskaka
      for (i = 0; i <4; i++) {
        j = i * 6;        
        debug.print(packets[i].id);
        debug.print(",");
        debug.print(regs[j]);
        debug.print(",");
        debug.print(regs[j+1]);
        debug.print(",");
        debug.print(regs[j+2]);
        debug.print(",");
        debug.print(regs[j+3]);
        debug.print(",");
        debug.print(regs[j+4]);
        debug.print(",");
        debug.println(regs[j+5]);
        //delay(100);
      }
      modbus_update();
      
      for (i = 4; i <43; i++) {
        j = i * 8;
        debug.print(packets[i].id);
        debug.print(",");
        debug.print(regs[j]);
        debug.print(",");
        debug.print(regs[j+1]);
        debug.print(",");
        debug.print(regs[j+2]);
        debug.print(",");
        debug.print(regs[j+3]);
        debug.print(",");
        debug.print(regs[j+4]);
        debug.print(",");
        debug.print(regs[j+5]);
        debug.print(",");
        debug.print(regs[j+6]);
        debug.print(",");
        debug.println(regs[j+7]);
      }
      modbus_update();
      

      debug.println("----------------------------");
  }//if end
}//loop end
  /*
    for (int i = 0; i < 8 ; i++) {
      debug.print(regs[i]);
      debug.print(',');
    }
    debug.println(" ");
    debug.println("--");
    debug.println(packets[0].failed_requests);
    //debug.println(packets[1].failed_requests);
    //    debug.println(packets[1].exception_errors);
    //    exception_errors
    debug.println("--");
    delay(200);
    ------------------------*/
  /*
  debug.println(" ");
  debug.println(" ");
  debug.println(" ");
  debug.println("Exception Errors: "+packets[i].exception_errors);
  delay(100);
  debug.println("Failed requests: "+packets[i].failed_requests);
  delay(100);

  delay(100);
  debug.println("Requests: "+packets[i].requests);
  delay(100);
  debug.println("Successful request: "+packets[i].successful_requests);

  //debug.println(regs[i]);
  }
  delay(500);
  //TODO: construct string and sent it out to serial
  /*for(i=0; i<=30; i++){

  j=i*4;

  sprintf(buff, "\n",packets[i].id," ",regs[j]," ",regs[j+1]," ",regs[j+2]," ",regs[j+2]);
  debug.println(buff);

  } */

// update the array with the counter data

/*regs[2] = packet1->requests;
regs[3] = packet1->successful_requests;
regs[4] = packet1->failed_requests;
regs[5] = packet1->exception_errors;
regs[6] = packet2->requests;
regs[7] = packet2->successful_requests;
regs[8] = packet2->failed_requests;
regs[9] = packet2->exception_errors;
Serial.println(packet1->requests);
  Serial.println(packet1->successful_requests);
  Serial.println(packet1->failed_requests);
  Serial.println(packet1->exception_errors);
*/



/*modbus_construct(&packets[PACKET2], 2, READ_HOLDING_REGISTERS, 0, 1, 4);
modbus_construct(&packets[PACKET3], 3, READ_HOLDING_REGISTERS, 0, 1, 8);
modbus_construct(&packets[PACKET4], 4, READ_HOLDING_REGISTERS, 0, 1, 12);
modbus_construct(&packets[PACKET5], 5, READ_HOLDING_REGISTERS, 0, 1, 16);
modbus_construct(&packets[PACKET6], 6, READ_HOLDING_REGISTERS, 0, 1, 20);
modbus_construct(&packets[PACKET7], 7, READ_HOLDING_REGISTERS, 0, 1, 24);
modbus_construct(&packets[PACKET8], 8, READ_HOLDING_REGISTERS, 0, 1, 28);
modbus_construct(&packets[PACKET9], 9, READ_HOLDING_REGISTERS, 0, 1, 32);
modbus_construct(&packets[PACKET10], 10, READ_HOLDING_REGISTERS, 0, 1, 36);
modbus_construct(&packets[PACKET11], 11, READ_HOLDING_REGISTERS, 0, 1, 40);
modbus_construct(&packets[PACKET12], 12, READ_HOLDING_REGISTERS, 0, 1, 44);
modbus_construct(&packets[PACKET13], 13, READ_HOLDING_REGISTERS, 0, 1, 48);
modbus_construct(&packets[PACKET14], 14, READ_HOLDING_REGISTERS, 0, 1, 52);
modbus_construct(&packets[PACKET15], 15, READ_HOLDING_REGISTERS, 0, 1, 56);
modbus_construct(&packets[PACKET16], 16, READ_HOLDING_REGISTERS, 0, 1, 60);
modbus_construct(&packets[PACKET17], 17, READ_HOLDING_REGISTERS, 0, 1, 64);
modbus_construct(&packets[PACKET18], 18, READ_HOLDING_REGISTERS, 0, 1, 68);
modbus_construct(&packets[PACKET19], 19, READ_HOLDING_REGISTERS, 0, 1, 72);
modbus_construct(&packets[PACKET20], 20, READ_HOLDING_REGISTERS, 0, 1, 76);
modbus_construct(&packets[PACKET21], 21, READ_HOLDING_REGISTERS, 0, 1, 80);
modbus_construct(&packets[PACKET22], 22, READ_HOLDING_REGISTERS, 0, 1, 84);
modbus_construct(&packets[PACKET23], 23, READ_HOLDING_REGISTERS, 0, 1, 88);
modbus_construct(&packets[PACKET24], 24, READ_HOLDING_REGISTERS, 0, 1, 92);
modbus_construct(&packets[PACKET25], 25, READ_HOLDING_REGISTERS, 0, 1, 96);
modbus_construct(&packets[PACKET26], 26, READ_HOLDING_REGISTERS, 1, 1, 100);
modbus_construct(&packets[PACKET27], 27, READ_HOLDING_REGISTERS, 0, 1, 104);
modbus_construct(&packets[PACKET28], 28, READ_HOLDING_REGISTERS, 0, 1, 108);
modbus_construct(&packets[PACKET29], 29, READ_HOLDING_REGISTERS, 0, 1, 112);
modbus_construct(&packets[PACKET30], 30, READ_HOLDING_REGISTERS, 0, 1, 114);



// Initialize each packet
//modbus_construct(packet,slaveid,command,reg address,datalen,local reg stor index)
//modbus_construct(&packets[PACKET1], 1, READ_COIL_STATUS, 0, 100, 0);

//   modbus_construct(&packets[PACKET1], 5, READ_HOLDING_REGISTERS, 0x3000, 8, 0);//invt 1-7
//Data:
//  modbus_construct(&packets[PACKET2], 8, READ_HOLDING_REGISTERS, 0x3211, 8, 0);//sb200 8- 15 (16 back up pump)
//Data:
//modbus_construct(&packets[PACKET2], 17, READ_HOLDING_REGISTERS, 0x40005, 8, 0);//ABB 17-21 (16 back up pump)
//Data:

//op frep
//set freq
//bus voltage
//output voltage
//current
//speed
//power
//torque

*/
