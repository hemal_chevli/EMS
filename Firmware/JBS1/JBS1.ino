//Date:20-Aug-15
//send modbus data via esp to webserver
//arduino 1.0.5

//Hardware
//Mega 2560
//serial interfaces
//modbus  Serial1
//debug   Serial0
//4 txEn 
//6 LED status of modbus activity
//esp8266(7,8) Serial2
//reset button
//Power LED
//SD card SPI
//RTC     I2C
//Time update from NTP to RTC

//send static data first and connected modbus
//wire rct
//wire sd card

#include <SimpleModbusMaster.h>
#include <Wire.h>
#include "EmonLib.h"                 
EnergyMonitor emon1;                 
EnergyMonitor emon2;                 
EnergyMonitor emon3;                 
EnergyMonitor emon4;                 
//#include <SD.h>
#define baud 19200 //modbus baud
//#define timeout 500
#define timeout 1000
#define polling 200 // the scan rate
//#define polling 200 // the scan rate
#define retry_count 1000 //tweek this
// used to toggle the receive/transmit pin on the driver
//#define TxEnablePin 2 //MEGA SHIELD
#define TxEnablePin 21
#define LED 13
// The total amount of available memory on the master to store data
#define TOTAL_NO_OF_REGISTERS 350
#define debugging true
// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS is automatically updated.
enum
{
  PACKET1,
  PACKET2,
  PACKET3,
  PACKET4,
  PACKET5,
  PACKET6,
  PACKET7,
  PACKET8,
  PACKET9,
  PACKET10,
  PACKET11,
  PACKET12,
  PACKET13,
  PACKET14,
  PACKET15,
  PACKET16,
  PACKET17,
  PACKET18,
  PACKET19,
  PACKET20,
  PACKET21,
  PACKET22,
  PACKET23,
  PACKET24,
  PACKET25,
  PACKET26,
  PACKET27,
  PACKET28,
  PACKET29,
  PACKET30,
  PACKET31,
  PACKET32,
  PACKET33,
  PACKET34,
  PACKET35,
  PACKET36,
  PACKET37,
  PACKET38,
  PACKET39,
  PACKET40,
  PACKET41,
  PACKET42,
  PACKET43,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];
// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];
/*MEGA SHIELD
DEBUG SERIAL
ESP SERIAL1
MODBUS SERIAL 2
*/
HardwareSerial & debug    = Serial;
HardwareSerial & modbus   = Serial3;
HardwareSerial & esp8266  = Serial2;

//AT commandsstored in flash
const char at[] PROGMEM=            "AT";
const char cipstatus[] PROGMEM =    "AT+CIPSTATUS";
const char rst[] PROGMEM =          "AT+RST";
const char cwjap[] PROGMEM =        "AT+CWJAP=\"EMS\",\"9824130336\"";
const char cipstart[] PROGMEM =     "AT+CIPSTART=\"TCP\",\"128.199.107.106\",80";
const char cipclose[] PROGMEM =     "AT+CIPCLOSE";
const char cifsr[] PROGMEM =        "AT+CIFSR";
const char cipmux[] PROGMEM =       "AT+CIPMUX=0";
//cwmode
//CIPMODE

//pointers to AT commands
const char*  AT PROGMEM = at;
const char*  CIPSTATUS PROGMEM = cipstatus;
const char*  RESET PROGMEM = rst;
const char*  CWJAP PROGMEM = cwjap;
const char*  CIPSTART PROGMEM = cipstart;
const char*  CIPCLOSE PROGMEM = cipclose;
const char*  CIFSR PROGMEM = cifsr;
const char*  CIPMUX PROGMEM = cipmux;
char cmdBuffer[100];

const char POST[] PROGMEM =           "POST /ems/demo_post.php HTTP/1.1";//32 
const char HOST[] PROGMEM =           "Host: pruthvi.co"; //16
const char contentType[] PROGMEM =    "Content-Type: application/json"; //30
const char contentLength[] PROGMEM =  "Content-Length: "; //16 +10(LF) + jsonlenlen(3)  +jsonlen(373)
const int postlen= 105; //104
const char* const POSTMethod[] PROGMEM = {POST, HOST, contentType, contentLength};

int cipsendLen=0;
int connection;
String json;
int i,j;
double Irms1;
double Irms2;
double Irms3;
double Irms4;


//Function Declaration
void createJSON(void);
void sendData(void);
byte countDigits(int num);

void setup()
{
emon1.current(A0, 54);             // Current: input pin, calibration.
emon2.current(A1, 54);             // Current: input pin, calibration.
emon3.current(A2, 65);             // Current: input pin, calibration.
emon4.current(A3, 60);             // Current: input pin, calibration.

  if(debugging) debug.begin(115200);
  esp8266.begin(115200);
  //espsetup
  sendATcommand("AT","OK",1000,debugging);
  
  delay(2000);//WAIT FOR MODULE TO CONNECT TO AP
  
  //get ip
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIFSR))); 
  sendATcommand(cmdBuffer,"OK",5000,debugging); //WILL PRINT 0.0.0.0 IF NOT CONNECTED

  //------YASKAWA----------------------
  modbus_construct(&packets[PACKET1], 27, READ_HOLDING_REGISTERS, 0x0023, 6, 0);
  modbus_construct(&packets[PACKET2], 28, READ_HOLDING_REGISTERS, 0x0023, 6, 6);
  modbus_construct(&packets[PACKET3], 29, READ_HOLDING_REGISTERS, 0x0023, 6, 12);
  modbus_construct(&packets[PACKET4], 30, READ_HOLDING_REGISTERS, 0x0023, 6, 18);
  //------SB-200------------
  modbus_construct(&packets[PACKET5],  31, READ_HOLDING_REGISTERS, 0x3211, 8, 32);
  modbus_construct(&packets[PACKET6],  32, READ_HOLDING_REGISTERS, 0x3211, 8, 40);
  modbus_construct(&packets[PACKET7],  33, READ_HOLDING_REGISTERS, 0x3211, 8, 48);
  modbus_construct(&packets[PACKET8],  34, READ_HOLDING_REGISTERS, 0x3211, 8, 56);
  modbus_construct(&packets[PACKET9],  35, READ_HOLDING_REGISTERS, 0x3211, 8, 64);
  modbus_construct(&packets[PACKET10], 36, READ_HOLDING_REGISTERS, 0x3211, 8, 72);
  modbus_construct(&packets[PACKET11], 37, READ_HOLDING_REGISTERS, 0x3211, 8, 80);
  modbus_construct(&packets[PACKET12], 38, READ_HOLDING_REGISTERS, 0x3211, 8, 88);
  //------INVT----------------------
  modbus_construct(&packets[PACKET13], 39, READ_HOLDING_REGISTERS, 0x3000, 8, 96);
  modbus_construct(&packets[PACKET14], 40, READ_HOLDING_REGISTERS, 0x3000, 8, 104);
  modbus_construct(&packets[PACKET15], 41, READ_HOLDING_REGISTERS, 0x3000, 8, 112);
  modbus_construct(&packets[PACKET16], 42, READ_HOLDING_REGISTERS, 0x3000, 8, 120);
  modbus_construct(&packets[PACKET17], 43, READ_HOLDING_REGISTERS, 0x3000, 8, 128);
  //------DELTA----------------------
  modbus_construct(&packets[PACKET18], 44, READ_HOLDING_REGISTERS, 0x2102, 7, 136);
  modbus_construct(&packets[PACKET19], 45, READ_HOLDING_REGISTERS, 0x2102, 7, 144);
  modbus_construct(&packets[PACKET20], 46, READ_HOLDING_REGISTERS, 0x2102, 7, 152);
  //------Stenter 2--------------------
  modbus_construct(&packets[PACKET21], 21, READ_HOLDING_REGISTERS, 0x2102, 7, 160);//DELTA-B
  modbus_construct(&packets[PACKET22], 15, READ_HOLDING_REGISTERS, 0x3000, 8, 168);//INVT
  modbus_construct(&packets[PACKET23], 16, READ_HOLDING_REGISTERS, 0x3000, 8, 176);//INVT
  modbus_construct(&packets[PACKET24], 17, READ_HOLDING_REGISTERS, 0x3000, 8, 184);//INVT
  modbus_construct(&packets[PACKET25], 18, READ_HOLDING_REGISTERS, 0x3000, 8, 192);//INVT
  modbus_construct(&packets[PACKET26], 19, READ_HOLDING_REGISTERS, 0x3000, 8, 200);//INVT
  modbus_construct(&packets[PACKET27], 20, READ_HOLDING_REGISTERS, 0x3000, 8, 208);//INVT
  modbus_construct(&packets[PACKET28], 22, READ_HOLDING_REGISTERS, 0x2102, 7, 216);//INVT
  modbus_construct(&packets[PACKET29], 23, READ_HOLDING_REGISTERS, 0x3000, 8, 224);//INVT
  modbus_construct(&packets[PACKET30], 24, READ_HOLDING_REGISTERS, 0x3000, 8, 232);//INVT
  modbus_construct(&packets[PACKET31], 25, READ_HOLDING_REGISTERS, 0x3000, 8, 240);//INVT
  //------Stenter 1--------------------
  modbus_construct(&packets[PACKET32], 2, READ_HOLDING_REGISTERS, 0x3000, 8, 248);//INVT
  modbus_construct(&packets[PACKET33], 3, READ_HOLDING_REGISTERS, 0x3000, 8, 256);//INVT
  modbus_construct(&packets[PACKET34], 4, READ_HOLDING_REGISTERS, 0x3000, 8, 264);//INVT
  modbus_construct(&packets[PACKET35], 5, READ_HOLDING_REGISTERS, 0x3000, 8, 272);//INVT
  modbus_construct(&packets[PACKET36], 6, READ_HOLDING_REGISTERS, 0x3000, 8, 280);//INVT
  modbus_construct(&packets[PACKET37], 7, READ_HOLDING_REGISTERS, 0x3000, 8, 288);//INVT
  modbus_construct(&packets[PACKET38], 8, READ_HOLDING_REGISTERS, 0x2102, 7, 296);//Delta
  modbus_construct(&packets[PACKET39], 9, READ_HOLDING_REGISTERS, 0x3211, 8, 304);//SB-200
  modbus_construct(&packets[PACKET40], 10, READ_HOLDING_REGISTERS, 0x3211, 8, 312);//SB-200
  modbus_construct(&packets[PACKET41], 11, READ_HOLDING_REGISTERS, 0x3211, 8, 320);//SB-200
  modbus_construct(&packets[PACKET42], 12, READ_HOLDING_REGISTERS, 0x3211, 8, 328);//SB-200
  modbus_construct(&packets[PACKET43], 13, READ_HOLDING_REGISTERS, 0x3211, 8, 336);//SB-200
  
  // Initialize the Modbus Finite State Machine
  modbus_configure(&Serial3, baud, SERIAL_8E1, timeout, polling, retry_count, TxEnablePin, packets, TOTAL_NO_OF_PACKETS, regs);

  pinMode(LED, OUTPUT);
  debug.println(F("\r\nGoing in Loop----------------------------------------------"));
}

void loop(){
  modbus_update();
  connection = connStatus();
  //STATUS:2 CONNECECTED TO WIFI AND HAS IP
  //STATUS:3 TCP CONNECTION ESTABLISHED IP AND PORT DISPLAYED
  //STATUS:4 DISCONNECTED FROM SERVER
  //STATUS:5 NOT CONNECTED TO AP
  //ALREAY CONNECTED
  //busy s...
  
  switch (connection) {
      case 2:
        //connected to AP and HAS IP
        //DO TCP START
        debug.println("case 2");
        //close previous connection
        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
        sendATcommand(cmdBuffer,"OK",2000,debugging);

        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTART))); 
        sendATcommand(cmdBuffer,"OK",20000,debugging);//CONNECT OK

        break;
      case 3:
        //SEND DATA
        modbus_update();
        sendData();
        break;
      case 4:
        // DICONNECTED
        // DO TCP START
        debug.println("case 4");
        //close
        //strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
        //sendATcommand(cmdBuffer,"OK",2000,debugging);
        //open
        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTART))); 
        sendATcommand(cmdBuffer,"OK",5000,debugging);
        break;
      case 5:
        debug.println("case 5");
        //NO AP
        //CONNECT TO AP
        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CWJAP))); 
        sendATcommand(cmdBuffer,"OK",5000,debugging);
        
        break;
      default:
      //close connection
        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
        sendATcommand(cmdBuffer,"OK",2000,debugging);
        break;
  }//switch end
}//loop end
byte countDigits(int num){
  byte count=0;
  while(num){
    num=num/10;
    count++;
  }
  return count;
} 
void sendData(void){
  
  int answer=0;
  //get current form CT
  Irms1 = emon1.calcIrms(1480);  // Calculate Irms only
  Irms2 = emon2.calcIrms(1480);  // Calculate Irms only
  Irms3 = emon3.calcIrms(1480);  // Calculate Irms only
  Irms4 = emon4.calcIrms(1480);  // Calculate Irms only

  createJSON();
  debug.println(json);
  int jsonLen = json.length();
  int jsonlenlen = countDigits(jsonLen);
  cipsendLen = jsonLen + jsonlenlen + postlen ; //when all zeros cipsend is 480
  sprintf(cmdBuffer, "AT+CIPSEND=%d", cipsendLen);
    //Serial.println(buf);
  sendATcommand(cmdBuffer,">",1000,debugging);
  for (int i = 0; i <=2 ; i++){
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[i]))); 
    esp8266.println(cmdBuffer);
    delay(50);
  }
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[3]))); 
  esp8266.print(cmdBuffer);
  esp8266.println(jsonLen);
  esp8266.println();
  delay(50);
  esp8266.print(json);
  delay(2000); //wait for reply
  //read SEND OK, if error resend
  sendATcommand(" ","SEND OK",5000,debugging);
  //while 10 sec call modbus and read if possible
  long sentTime= millis();
  while(millis()-sentTime<10000){
    modbus_update();
    //read reply here
  }
  //close connection
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
  sendATcommand(cmdBuffer,"OK",2000,debugging);
  //delay(5000);
}
int8_t connStatus(){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTATUS))); 
    esp8266.println(cmdBuffer); //send cipstatus command
    
    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
        if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
            response[x] = esp8266.read();
            if(debugging){
                debug.print(response[x]);
            }
            x++;
            if      (strstr(response, "STATUS:2") != NULL)    
            {
                answer = 2;
            }
            else if (strstr(response, "STATUS:3") != NULL)  
            {
                answer = 3;
            }
            else if (strstr(response, "STATUS:4") != NULL)  
            {
                answer = 4;
            }
            else if (strstr(response, "STATUS:5") != NULL)  
            {
                answer = 5;
            }
            
        }
    }while((answer == 0) && ((millis() - previous) < 5000));    // Waits for the asnwer with time out

    return answer;
}
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    if (ATcommand[0] != '\0')
    {
    esp8266.println(ATcommand);    // Send the AT command 
    }

    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
    if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
        response[x] = esp8266.read();
        if(isDebug && debugging){
            debug.print(response[x]);
        }
        x++;
        if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 1;
        }
        else if
        (strstr(response, "ERROR") != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 0;
            break;
        }
    }
    }while((answer == 0) && ((millis() - previous) < Timeout));    // Waits for the asnwer with time out

return answer;
}


void createJSON(void){
  json="{";
  for(i= 0 ;i<4;i++){
    j = i * 6;
    json+="\"";
    json+= packets[i].id;
    json+="\":";
    json+="[";
    json+=packets[i].id;
    json+=",";
    json+= regs[j];
    json+=",";
    json+= regs[j+1];
    json+=",";
    json+= regs[j+2];
    json+=",";
    json+= regs[j+3];
    json+=",";
    json+= regs[j+4];
    json+=",";
    json+= regs[j+5];
    json+="],";
    }
  for(i= 4 ;i<20;i++){
    j = i * 8;
    json+="\"";
    json+= packets[i].id;
    json+="\":";
    json+="[";
    json+=packets[i].id;
    json+=",";
    json+= regs[j];
    json+=",";
    json+= regs[j+1];
    json+=",";
    json+= regs[j+2];
    json+=",";
    json+= regs[j+3];
    json+=",";
    json+= regs[j+4];
    json+=",";
    json+= regs[j+5];
    json+=",";
    json+= regs[j+6];
    json+=",";
    json+= regs[j+7];
    json+="],";
    }
    for(i= 31 ;i<43;i++){
    j = i * 8;
    json+="\"";
    json+= packets[i].id;
    json+="\":";
    json+="[";
    json+=packets[i].id;
    json+=",";
    json+= regs[j];
    json+=",";
    json+= regs[j+1];
    json+=",";
    json+= regs[j+2];
    json+=",";
    json+= regs[j+3];
    json+=",";
    json+= regs[j+4];
    json+=",";
    json+= regs[j+5];
    json+=",";
    json+= regs[j+6];
    json+=",";
    json+= regs[j+7];
    json+="],";
    }
  //,"I1":9.33,"I2":12.31,"I4":12.31,"I4":12.31}
  json+="\"50\":[50,";
  json+=Irms1;
  json+="],\"51\":[51,";
  json+=Irms2;
  json+="],\"52\":[52,";
  json+=Irms3;
  json+="],\"53\":[53,";
  json+=Irms4;
  json+="]";
  json+="}";
}
/*
void createJSON(void){
  json="{";
  for(i= 0 ;i<15;i++){
    j = i * 8;
    json+="\"";
    json+= packets[i].id;
    json+="\":";
    json+="[";
    json+=packets[i].id;
    json+=",";
    json+= regs[j];
    json+=",";
    json+= regs[j+1];
    json+=",";
    json+= regs[j+2];
    json+=",";
    json+= regs[j+3];
    json+=",";
    json+= regs[j+4];
    json+=",";
    json+= regs[j+5];
    json+=",";
    json+= regs[j+6];
    json+=",";
    json+= regs[j+7];
    json+="],";
    }
  }
  //,"I1":9.33,"I2":12.31,"I4":12.31,"I4":12.31}
  json+=",\"I1\":";
  json+=Irms1;
  json+=",\"I2\":";
  json+=Irms1;
  json+=",\"I3\":";
  json+=Irms1;
  json+=",\"I4\":";
  json+=Irms1;
  json+="}";
}*/

/*
WORKING POST LOOP SNIPPET
//do switch on cipstatus
  if(sendATcommand("AT+CIPSTATUS","STATUS:4",1000,debugging)){
      sendATcommand("AT+CIPSTART=\"TCP\",\"50.87.144.195\",80","CONNECT",1000,debugging);
  }
  if(sendATcommand("AT+CIPSTATUS","STATUS:3",1000,debugging)){//CONNECTED TO WEBSERVER
    if(sendATcommand("AT+CIPSEND=152",">",1000,debugging)){
      esp8266.println("POST /sandbox/posttest.php HTTP/1.1");
      esp8266.println("Host: black-electronics.com");
      esp8266.println("Content-Type: application/x-www-form-urlencoded");
      esp8266.println("Content-Length: 15");
      esp8266.println("");
      esp8266.println("var_a=3&var_b=5"); 
      //read reply
      sendATcommand("","200 OK",10000,debugging);
      }
    }
    delay(3000);
---------------------
if(esp8266.)
        response[x] = esp8266.read();
        if(debugging){
            debug.print(response[x]);
        }
        x++;
        if (strstr(response, "+IPD") != NULL)    // check if the desired answer (OK) is in the response of the module
        {
          debug.println("Got response");
          answer = 1;
        }
        //process number
        if(answer = 1){
        switch (response[x]){
            case ':': //end of number  
              bytesReceived = receivedNumber;
              break;

            case ',': //start of number
              receivedNumber = 0; 
              break;
              
            case '0' ... '9': 
              receivedNumber *= 10;
              receivedNumber += response[x] - '0';
              break;
            } // end of switch  
          }  
        
          if(receivedNumber != 0){
      for (int i = 0; i < bytesReceived; i++)
      {
        debug.write(esp8266.read());
        delay(10);
      }
    }

*/
